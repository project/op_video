<?php


function op_video_field_info() {

  return array(
    'op_video' => array('label' => "OpenPackage Video")
  );
}


function _op_video_get_params_id($field) {
	return variable_get('op_video_params_id_' . $field['field_name'], '');
}


function _op_video_field_load_params($field) {
	$params_id = _op_video_get_params_id($field);
	
	if(!$params_id) {
		$params_id = _op_video_transcoding_params_insert();
		variable_set('op_video_params_id_' . $field['field_name'], $params_id);
	}
	
	return _op_video_transcoding_params_load($params_id);
}


function _op_video_field_retranscode_all($field) {

	$multiple = db_result(db_query("
		SELECT multiple
		FROM {node_field}
		WHERE field_name = '%s'
	",
		$field['field_name']
	));

//	if($field['multiple'])
	if($multiple)
		$table = 'content_' . $field['field_name'];
	else
		$table = 'content_type_' . $field['type_name'];
		
	$video_id_field = $field['field_name'] . '_video_id';

/*
	db_query("
		UPDATE {{$table}}, {op_videos}, {op_video_files}
		SET {op_videos}.secret = '%s',
			{op_videos}.status = 'dirty', 
			{op_videos}.upload_failed = 0,
			{op_videos}.uploaded = 0
		WHERE {{$table}}.$video_id_field = {op_videos}.video_id AND
			{op_videos}.source_file_id = {op_video_files}.file_id AND
			{op_video_files}.deleted = 0
	",
		md5(user_password())
	);
*/

	$result = db_query("
		SELECT {op_videos}.video_id
		FROM {op_videos}
		INNER JOIN {{$table}}
			ON {op_videos}.video_id = {{$table}}.$video_id_field
		INNER JOIN {op_video_files}
			ON {op_videos}.source_file_id = {op_video_files}.file_id
		WHERE {op_video_files}.deleted = 0
	");

	while($object = db_fetch_object($result)) {
		
		db_query("
			UPDATE {op_videos}
			SET secret = '%s',
				status = 'dirty', 
				upload_failed = 0,
				uploaded = 0
			WHERE video_id = $object->video_id
		",
			md5(user_password())
		);
	}

	$msg = t('Retranscoding all field %label videos.', array('%label' => $field['label']));
	drupal_set_message($msg);
	watchdog('op_video', $msg);
}


function _op_video_field_store_params($field) {
	$params_id = _op_video_get_params_id($field);
	_op_video_transcoding_params_update($params_id, $field);
	
	if($field['retranscode_all']) {
		_op_video_field_retranscode_all($field);
	}
}


function op_video_field_settings($op, $field) {

  switch ($op) {

    case 'form':
     	$form['preview_image_settings'] = _op_video_preview_image_settings_fieldset($field);
     	$form['player_settings'] = _op_video_player_settings_fieldset($field);
    	
    	$params = _op_video_field_load_params($field);
    	$form['transcoding_params'] = _op_video_transcoding_params_fieldset($params);
    	
			return $form;

    case 'validate':
    	_op_video_preview_image_settings_validate($field);
    	_op_video_player_settings_validate($field);
    	_op_video_transcoding_params_validate($field);
    	
    	if(!form_get_errors()) {
	    	_op_video_field_store_params($field);
    	}
			return;

    case 'save':
			_op_video_purge_image_cache();
			$preview_image_fields = _op_video_player_settings_fields();
			$player_fields = _op_video_preview_image_settings_fields();
			return array_merge($preview_image_fields, $player_fields);

    case 'database columns':

      return array(
				'video_id' => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE)
      );
      
		case 'tables':
		
			$tables = content_views_field_tables($field);
			
			$table_name = array_shift(array_keys($tables));
			$field_name = array_shift(array_keys($tables[$table_name]['fields']));
			
			unset($tables[$table_name]['fields']);
			
			$tables[$field['field_name'] . '_op_videos'] = array(
				'name' => 'op_videos',
				'join' => array(
					'left' => array(
						'table' => $table_name,
						'field' => $field_name
					),
					'right' => array(
						'field' => 'video_id'
					)
				),
		    'fields' => _op_video_views_op_videos_fields($field['widget']['label'])
		  );

			$tables[$field['field_name'] . '_op_video_files'] = _op_video_views_op_video_files_table($field['field_name'], $field['widget']['label']);
			
			return $tables;
  }
}

/*
function _op_video_get_field_video_id($node, $field) {

	if($field['multiple']) {
		return db_result(db_query("
			SELECT %s
			FROM {%s}
			WHERE vid = %d
		",
			$field['field_name'] . '_id',
			'content_field_' . $node->type,
			$node->vid
		));
	}
	else {
		return db_result(db_query("
			SELECT %s
			FROM {%s}
			WHERE vid = %d
		",
			$field['field_name'] . '_id',
			'content_type_' . $node->type,
			$node->vid
		));
	}
}
*/

function op_video_field($op, &$node, $field, &$node_field, $teaser, $page) {

	switch ($op) {

		case 'insert':
			$params_id = _op_video_get_params_id($field);
			
			foreach($node_field as $delta => $item) {
			
//				var_dump($item);

				if(!$node_field[$delta]['video_id']) {	// for node_import
					$node_field[$delta]['video_id'] = 
					_op_video_create_video($item['source_file_id'], $params_id, 
						$item['preview_image']['preview_image'], $item['custom_image_file_id']);
				}
			}
			
			break;
			
		case 'update':
			foreach($node_field as $delta => $item) {
				$video = _op_video_load($item['video_id']);
				
//		var_dump($item);
				
				if($item['upload']['delete']) {
					_op_video_delete($video);
					$node_field[$delta]['video_id'] = 0;
				}
				else if($item['source_file_id']) {
				
					$custom_image_file_id = $item['custom_image_file_id'] ? 
						$item['custom_image_file_id'] : $video->custom_image_file_id;
				
					$params_id = _op_video_get_params_id($field);
					$node_field[$delta]['video_id'] = 
						_op_video_create_video($item['source_file_id'], $params_id, 
							$item['preview_image']['preview_image'], $custom_image_file_id);
					
					if($custom_image_file_id == $video->custom_image_file_id) {
						$video->custom_image_file_id = NULL;
						unset($video->custom_image_file);
					}
					
					_op_video_delete($video);
				}
				else {
				
					if($item['preview_image']['preview_image'] != $video->preview_image) {
						_op_video_set_preview_image($item['video_id'], $item['preview_image']['preview_image']);
					}
					
					if($item['custom_image_file_id']) {
						_op_video_update_custom_image($video, $item['custom_image_file_id']);
					}
				
					if($item['retranscode']) {
						_op_video_retranscode_video($item['video_id']);
					}
				}
			}

			break;

		case 'delete':
			foreach($node_field as $item)
				_op_video_delete($item['video_id']);
			break;
	}
}


function op_video_widget_info() {

  return array(
    'upload' => array(
      'label' => t('Upload'),
      'field types' => array('op_video'),
    ),
    'filepath' => array(
      'label' => t('Filepath'),
      'field types' => array('op_video'),
    )
  );
}


function op_video_widget_settings($op, $widget) {

  switch ($op) {

    case 'form':
      return $form;

    case 'save':
    	return array();

    case 'callbacks':
      return array(
        'view' => CONTENT_CALLBACK_CUSTOM,
      );
  }
}


function _op_video_widget_filepath($op, &$node, $field, &$node_field) {

  switch($op) {

		case 'prepare form values':
			foreach($node_field as $delta => $item) {
				$video = _op_video_load($item['video_id']);
				$node_field[$delta]['filepath'] = $video->source_file->filepath;
				$node_field[$delta]['deleted'] = $video->source_file->deleted;
			}
			break;

    case 'form':
    
  		_op_video_create_dirs();
    
			$form[$field['field_name']] = array(
        '#tree' => TRUE,
        '#weight' => $field['widget']['weight'],
			);

      if($field['multiple']) {
        $form[$field['field_name']]['#type'] = 'fieldset';
        $form[$field['field_name']]['#title'] = t($field['widget']['label']);
        $form[$field['field_name']]['#description'] = $field['widget']['description'];

        foreach(range(0, 3) as $delta) {
          $form[$field['field_name']][$delta]['filepath'] = array(
            '#type' => 'textfield',
            '#title' => '',
						'#autocomplete_path' => 'op_video/autocomplete',
            '#default_value' => isset($node_field[$delta]['filepath']) ? $node_field[$delta]['filepath'] : $field['widget']['default_value'][$delta]['filepath']
          );
 
					if($field['required'] && $delta == 0)
						$form[$field['field_name']][$delta]['filepath']['#required'] = TRUE;
						
					$form[$field['field_name']][$delta]['video_id'] = array(
	          '#type' => 'value',
	          '#value' => $node_field[$delta]['video_id']
	        );

					if(user_access('administer video content') && 
						isset($node_field[$delta]['filepath']) && 
						!$node_field[$delta]['deleted']) {
						
						$form[$field['field_name']][$delta]['retranscode'] = array(
							'#type' => 'checkbox', 
							'#title' => t('Retranscode this video.'), 
						);
					}
				}
      }
      else {
        $form[$field['field_name']][0]['filepath'] = array(
          '#type' => 'textfield',
          '#title' => $field['widget']['label'],
					'#autocomplete_path' => 'op_video/autocomplete',
          '#default_value' => isset($node_field[0]['filepath']) ? $node_field[0]['filepath'] : $field['widget']['default_value'][0]['filepath'],
          '#required' => $field['required'],
          '#description' => isset($field['widget']['description']) ? $field['widget']['description'] : '',
        );
        
				$form[$field['field_name']][0]['keepme'] = array(
          '#type' => 'value',
          '#value' => TRUE
        );
        
        $form[$field['field_name']][0]['video_id'] = array(
          '#type' => 'value',
          '#value' => $node_field[0]['video_id']
        );
        
				if(user_access('administer video content') && 
					isset($node_field[0]['filepath']) && 
					!$node_field[0]['deleted']) {
					
					$form[$field['field_name']][0]['retranscode'] = array(
						'#type' => 'checkbox', 
						'#title' => t('Retranscode this video.'), 
					);
				}
      }

      return $form;
      
		case 'validate':
			foreach($node_field as $delta => $item) {
				if($item['filepath'] != '' &&
					!is_file($item['filepath']) &&
					!is_link($item['filepath'])) {
					
					form_set_error($field['field_name'], 'File not found.');
				}
			}
			break;
			
		case 'submit':

			foreach($node_field as $delta => $item) {
				$node_field[$delta]['file_id'] = _op_video_insert_filepath($item['filepath'], TRUE);
			}
			
			break;
			
	}
}


function _op_video_widget_upload_form_item($name, $existing, $post) {

	if(!count($_POST) || $post['delete']) {

		// this is a fresh form - delete old preview video if it exists

		$file = file_save_upload($name, file_directory_temp());

		if($file)
			file_delete($file->filepath);
	}

	$file = file_check_upload($name);

	if(!$post['delete'] && ($existing || $file))
		$existing = TRUE;
	else
		$existing = FALSE;

	$item = array(
    '#tree' => TRUE,
	);

	$item[$name] = array(
		'#type' => 'file', 
		'#name' => 'files[' . $name . ']',
		'#size' => 60,
	//	'#required' => $field['required'],		// this doesn't work for #type 'file'
		'#description' => 'Max size: ' . _op_video_upload_max_size()
	);

	if(!$existing)
		$item[$name]['#title'] = t('Upload file');
	else
		$item[$name]['#title'] = t('Upload new file');

	if($existing && !$field['required']) {
    $item['or'] = array(
      '#value' => '<p>' . t('or') . '</p>',
    );

    $item['delete'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete uploaded file'),
    );

    $item['or leave it alone'] = array(
      '#value' => '<p>' . t('or leave it alone.') . '</p>',
    );
	}
	
	return $item;
}


function _op_video_widget_upload_validate($name, $post) {

	$file = file_check_upload($name);

	if($file && !$post['delete']) {
	
		if($_SESSION['file_uploads'][$name]->filepath != $file->filepath)
			file_delete($_SESSION['file_uploads'][$name]->filepath);
	}
	
	return $file;
}


function _op_video_widget_preview_image($field, $node_field) {

	if(!$field['preview_image_user_selectable'])
		return;

	$item = array(
		'#type' => 'fieldset',
		'#title' => t('Preview image')
	);
	
	if($node_field[0]['video_id']) {
		$video = _op_video_load($node_field[0]['video_id']);
	}
	
	if(_op_video_does_file_exist($video->start_image_file))
		$options['start'] = _op_video_render_preview_image($video, 80, 60, NULL, 'start') . ' - start of video';
	else
		$options['start'] = 'Start of video';
	
	if(_op_video_does_file_exist($video->third_image_file))
		$options['third'] = _op_video_render_preview_image($video, 80, 60, NULL, 'third') . ' - @ 1/3 of video duration';
	else
		$options['third'] = '@ 1/3 of video duration';

	$existing = _op_video_does_file_exist($video->custom_image_file);

	if($existing)
		$options['custom'] = _op_video_render_preview_image($video, 80, 60, NULL, 'custom') . ' - custom image';
	else
		$options['custom'] = 'Custom image';

	$item['preview_image'] = array(
		'#type' => 'radios',
		'#default_value' => isset($node_field[0]['preview_image']) ? $node_field[0]['preview_image'] : $field['preview_image_image'],
		'#options' => $options
	);

	$post = $_POST['preview_image'];
	$item['image_upload'] = _op_video_widget_upload_form_item($field['field_name'] . '_image', $existing, $post);

	return $item;
}


function _op_video_widget_upload($op, &$node, $field, &$node_field) {

	$post = $_POST['upload'];
	$delete = $post['delete'];

  switch ($op) {

		case 'prepare form values':
			foreach($node_field as $delta => $item) {
				$video = _op_video_load($item['video_id']);
				$node_field[$delta]['preview_image'] = $video->preview_image;
			}
			break;

    case 'form':

  		_op_video_create_dirs();
  		
			$form[$field['field_name']]['#weight'] = $field['widget']['weight'];
			$form[$field['field_name']]['#tree'] = TRUE;
  		$form[$field['field_name']][0] = array(
  			'#type' => 'fieldset',
				'#title' => t($field['widget']['label'])
  		);
			
			$existing = $node_field[0]['video_id'];
			
  		$form[$field['field_name']][0]['upload'] = _op_video_widget_upload_form_item($field['field_name'], $existing, $post);

      $form[$field['field_name']][0]['video_id'] = array(
        '#type' => 'value',
        '#value' => $node_field[0]['video_id']
      );

      $form[$field['field_name']][0]['preview_image'] = _op_video_widget_preview_image($field, $node_field);

			$form['#attributes']['enctype'] = 'multipart/form-data';
			
//			var_dump($form);
			
      return $form;


		case 'validate':

			$file = _op_video_widget_upload_validate($field['field_name'], $post);

			if(!$file) {
				if(!$node_field[0]['video_id'] && $field['required'])
					form_set_error($field['field_name'], 'You need to upload a video.');
			}

			break;


		case 'process form values':

			static $pfv;

			if(!$pfv[$field['field_name']]) {
				$pfv[$field['field_name']] = TRUE;
				break;
			}
			
//			var_dump($field, $node_field);
			
			if($node->op == t('Preview')) {

				if(!$delete) {
					$file = file_save_upload($field['field_name']);
					$node_field[0]['filepath'] = $file->filepath;
				}
				
				if(!$_POST['preview_image']['delete']) {
					$file = file_save_upload($field['field_name'] . '_image');
					$node_field[0]['image_filepath'] = $file->filepath;
				}
			}

			break;


		case 'submit':

			$file = file_check_upload($field['field_name']);

/*
			if($node->nid && !$file && !$delete) { 
				$old_node = (array)node_load($node->nid);
				$old_video = _op_video_load($old_node[$field['field_name']][0]['video_id']);
				$old_file = _op_video_get_file_info($old_video->source_file_id);
				$node_field[0]['filepath'] = $old_file->filepath;
			}
*/	
			if($file) {
				$node_field[0]['source_file_id'] = _op_video_save_upload($field['field_name'], 'original');
			}

			$file = file_check_upload($field['field_name'] . '_image');

			if($file) {
				$node_field[0]['custom_image_file_id'] = _op_video_save_upload($field['field_name'] . '_image', 'original');
			}

			break;
	}
}


function op_video_widget($op, &$node, $field, &$node_field) {

	switch($field['widget']['type']) {

		case 'filepath':
			return _op_video_widget_filepath($op, $node, $field, $node_field);

		case 'upload':
			return _op_video_widget_upload($op, $node, $field, $node_field);
	}
}


function op_video_field_formatter_info() {

	return array(
		'default' => array(
			'label' => 'Inline player',
			'field types' => array('op_video'),
		),
		'preview-image-layer' => array(
			'label' => 'Preview image - play in layer',
			'field types' => array('op_video'),
		),
		'preview-image-link' => array(
			'label' => 'Preview image - link to node',
			'field types' => array('op_video'),
		)
	);
}


function op_video_field_formatter($field, $item, $formatter, $node) {

	_op_video_set_render_settings($field);
	
	$video = _op_video_load($item['video_id']);
	
	return _op_video_render_video($video, $formatter, $node->nid, FALSE);
}


function _op_video_get_first_video($node) {
	$content_type_info = _content_type_info();
	
	foreach((array)$node as $field => $value) {
		if($content_type_info['fields'][$field]['type'] == 'op_video') {
    	return _op_video_load($value[0]['video_id']);
    }
	}
}


function op_video_nodeapi(&$node, $op, $teaser, $page) {

	if($op != 'rss item')
		return;
		
	$video = _op_video_get_first_video($node);

	if($video && !$video->source_file->deleted) {

		$rss_items[] = array(
			'key' => 'enclosure',
			'attributes' => array(
				'url' => file_create_url($video->source_file->filepath),
				'length' => $video->source_file->filesize,
				'type' => $video->source_file->filemime
			)
		);
		
    return $rss_items;
	}
}


