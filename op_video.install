<?php

// Copyright 2007 Jonathan Brown

function op_video_install() {

	$path = drupal_get_path('module', 'op_video');
	require_once './' . $path . '/op_video.module';

	_op_video_create_dirs();

	switch ($GLOBALS['db_type']) {
		case 'mysql':
		case 'mysqli':

			db_query("
				CREATE TABLE {op_videos} (
					video_id INT(10) UNSIGNED NOT NULL auto_increment,
					duration INT(10) UNSIGNED NOT NULL DEFAULT '0',
					secret CHAR(32) NOT NULL,
					source_file_id INT(10) UNSIGNED NOT NULL,
					flv_file_id INT(10) UNSIGNED NULL DEFAULT NULL,
					start_image_file_id INT(10) UNSIGNED DEFAULT NULL,
					third_image_file_id INT(10) UNSIGNED DEFAULT NULL,
					custom_image_file_id INT(10) UNSIGNED DEFAULT NULL,
					preview_image enum('start','third','custom') NOT NULL DEFAULT 'third',
					current_params_id INT(10) UNSIGNED NOT NULL DEFAULT '0',
					dirty_params_id INT(10) UNSIGNED NOT NULL DEFAULT '0',
					status ENUM('dirty','uploaded','idle') NOT NULL DEFAULT 'dirty',
					error ENUM('none','file not found','connection error','invalid credentials','insufficient credit','transcoding failed') NOT NULL DEFAULT 'none',
					upload_failed INT(10) UNSIGNED NOT NULL DEFAULT '0',
					uploaded INT(10) UNSIGNED NOT NULL DEFAULT '0',
					transcoder_version TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
					views INT(10) UNSIGNED NOT NULL DEFAULT '0',
					views_completed INT(10) UNSIGNED NOT NULL DEFAULT '0',
					downloads INT(10) UNSIGNED NOT NULL DEFAULT '0',
					PRIMARY KEY (video_id),
					UNIQUE KEY flv_file_id (flv_file_id),
					UNIQUE KEY raw_image_1_file_id (raw_image_1_file_id),
					UNIQUE KEY raw_image_2_file_id (raw_image_2_file_id),
					UNIQUE KEY splash_file_id (splash_file_id),
					UNIQUE KEY thumb_file_id (thumb_file_id),
					KEY status (status),
					KEY transcoder_version (transcoder_version),
					KEY views (views),
					KEY downloads (downloads),
					KEY error (error),
					KEY secret (secret),
					KEY views_completed (views_completed),
					KEY duration (duration)				
				)
			");

			db_query("
				CREATE TABLE {op_video_files} (
					file_id INT(10) UNSIGNED NOT NULL auto_increment,
					filename VARCHAR(255) NOT NULL,
					filepath VARCHAR(255) NOT NULL,
					filemime VARCHAR(255) NOT NULL,
					filesize BIGINT(20) UNSIGNED NOT NULL NULL DEFAULT '0',
					keepme TINYINT(1) UNSIGNED NOT NULL NULL DEFAULT '0',
					dirty TINYINT(1) UNSIGNED NOT NULL NULL DEFAULT '0',
					obtained INT(10) UNSIGNED NOT NULL NULL DEFAULT '0',
					deleted INT(10) UNSIGNED NOT NULL NULL DEFAULT '0',
					PRIMARY KEY (file_id),
					KEY obtained (obtained),
					KEY filesize (filesize),
					KEY filepath (filepath),
					KEY deleted (deleted)
				)
			");

			db_query("
				CREATE TABLE {op_video_params} (
					params_id INT(10) UNSIGNED NOT NULL auto_increment,
					width SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
					frame_rate TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
					key_interval decimal(3,1) UNSIGNED NOT NULL DEFAULT '0.0',
					video_bitrate SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
					audio_channels ENUM('0','1','2') NOT NULL DEFAULT '0',
					audio_sample_rate ENUM('11025','22050','44100') NOT NULL DEFAULT '11025',
					audio_bitrate SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
					max_duration SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
					keep_source_file TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
					PRIMARY KEY (params_id)
				)
			");
			
			db_query("
					CREATE TABLE op_video_image_cache (
						video_id INT(10) UNSIGNED NOT NULL,
						image ENUM('start','third','custom') collate utf8_bin NOT NULL,
						width SMALLINT(5) UNSIGNED NOT NULL,
						height SMALLINT(5) UNSIGNED NOT NULL,
						file_id INT(10) UNSIGNED NOT NULL,
						PRIMARY KEY (video_id, image, width, height)
					)
			");

			db_query("
				CREATE TABLE {op_video_stats} (
					type ENUM('view', 'view completed', 'download') NOT NULL,
					video_id INT(10) UNSIGNED NOT NULL,
					uid INT(10) UNSIGNED NOT NULL,
					sid CHAR(32) NOT NULL,
					ip_addr INT(10) UNSIGNED NOT NULL,
					timestamp INT(10) UNSIGNED NOT NULL,
					KEY video_id (video_id),
					KEY timestamp (timestamp),
					KEY uid (uid),
					KEY type (type)
				)
			");

			break;

		case 'pgsql':
			return;
	}
}


function _op_video_convert_timestamp_to_int(&$ret, $table, $field) {

	$ret[] = update_sql("
		ALTER TABLE {{$table}}
		ADD temp INT(10) UNSIGNED NOT NULL
	");
	
	$ret[] = update_sql("
		UPDATE {{$table}}
		SET temp = UNIX_TIMESTAMP($field)
	");

	$ret[] = update_sql("
		ALTER TABLE {{$table}}
		CHANGE $field $field INT(10) UNSIGNED NOT NULL DEFAULT '0'
	");

	$ret[] = update_sql("
		UPDATE {{$table}}
		SET $field = temp
	");

	$ret[] = update_sql("
		ALTER TABLE {{$table}}
		DROP temp
	");
}


function op_video_update_1() {

	switch($GLOBALS['db_type']) {
		case 'mysql':
		case 'mysqli':
		
			$ret[] = update_sql("
				ALTER TABLE {op_videos}
				CHANGE duration duration INT(10) UNSIGNED NOT NULL DEFAULT '0',
				CHANGE source_file_id source_file_id INT(10) UNSIGNED NOT NULL,
				CHANGE flv_file_id flv_file_id INT(10) UNSIGNED NULL DEFAULT NULL,
				CHANGE raw_image_1_file_id raw_image_1_file_id INT(10) UNSIGNED NULL DEFAULT NULL,
				CHANGE raw_image_2_file_id raw_image_2_file_id INT(10) UNSIGNED NULL DEFAULT NULL,
				CHANGE splash_file_id splash_file_id INT(10) UNSIGNED NULL DEFAULT NULL,
				CHANGE thumb_file_id thumb_file_id INT(10) UNSIGNED NULL DEFAULT NULL,
				CHANGE current_params_id current_params_id INT(10) UNSIGNED NOT NULL DEFAULT '0',
				CHANGE dirty_params_id dirty_params_id INT(10) UNSIGNED NOT NULL DEFAULT '0',
				CHANGE transcoder_version transcoder_version TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
				CHANGE views views INT(10) UNSIGNED NOT NULL DEFAULT '0',
				CHANGE views_completed views_completed INT(10) UNSIGNED NOT NULL DEFAULT '0',
				CHANGE downloads downloads INT(10) UNSIGNED NOT NULL DEFAULT '0'
			");
			
			_op_video_convert_timestamp_to_int($ret, 'op_videos', 'upload_failed');
			_op_video_convert_timestamp_to_int($ret, 'op_videos', 'uploaded');

			$ret[] = update_sql("
				ALTER TABLE {op_video_files}
				CHANGE filesize filesize BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
				CHANGE keepme keepme TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
				CHANGE dirty dirty TINYINT(1) UNSIGNED NOT NULL DEFAULT '0'
			");

			_op_video_convert_timestamp_to_int($ret, 'op_video_files', 'obtained');
			_op_video_convert_timestamp_to_int($ret, 'op_video_files', 'deleted');
			
			$ret[] = update_sql("
				ALTER TABLE {op_video_params}
				CHANGE width width SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
				CHANGE frame_rate frame_rate TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
				CHANGE key_interval key_interval DECIMAL(3, 1) UNSIGNED NOT NULL DEFAULT '0.0',
				CHANGE video_bitrate video_bitrate SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
				CHANGE audio_bitrate audio_bitrate SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
				CHANGE keep_source_file keep_source_file TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
				ADD max_duration SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0' AFTER audio_bitrate
			");
			
			$ret[] = update_sql("
				ALTER TABLE {op_video_stats}
				CHANGE type type ENUM('view', 'view completed', 'download') NOT NULL
			");
			
			_op_video_convert_timestamp_to_int($ret, 'op_video_stats', 'timestamp');

			break;
	}

	return $ret;
}


function op_video_update_2() {

	switch($GLOBALS['db_type']) {
		case 'mysql':
		case 'mysqli':
		
			$ret[] = update_sql("
				ALTER TABLE {op_videos}
				CHANGE raw_image_1_file_id start_image_file_id INT(10) UNSIGNED NULL DEFAULT NULL,
				CHANGE raw_image_2_file_id third_image_file_id INT(10) UNSIGNED NULL DEFAULT NULL,
				DROP splash_file_id,
				DROP thumb_file_id,
				ADD custom_image_file_id INT(10) UNSIGNED DEFAULT NULL,
				ADD preview_image enum('start','third','custom') NOT NULL DEFAULT 'third'
			");

			$ret[] = update_sql("
				CREATE TABLE op_video_image_cache (
					video_id INT(10) UNSIGNED NOT NULL,
					image ENUM('start','third','custom') collate utf8_bin NOT NULL,
					width SMALLINT(5) UNSIGNED NOT NULL,
					height SMALLINT(5) UNSIGNED NOT NULL,
					file_id INT(10) UNSIGNED NOT NULL,
					PRIMARY KEY (video_id, image, width, height)
				)
			");

			break;
	}
	
	return $ret;
}


function op_video_uninstall() {
  db_query('DROP TABLE {op_videos}');
  db_query('DROP TABLE {op_video_files}');
  db_query('DROP TABLE {op_video_params}');
  db_query('DROP TABLE {op_video_image_cache}');
  db_query('DROP TABLE {op_video_stats}');
}

