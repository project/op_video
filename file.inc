<?php

// Copyright 2007 Jonathan Brown

function _op_video_videos_path() {
	return file_directory_path() . '/videos';
}


function _op_video_temp_path() {
	return _op_video_videos_path() . '/temp';
}


function _op_video_original_path() {
	return _op_video_videos_path() . '/original';
}


function _op_video_transcoded_path() {
	return _op_video_videos_path() . '/transcoded';
}


function _op_video_image_cache_path() {
	return _op_video_videos_path() . '/image-cache';
}


function _op_video_create_dirs() {

	$failed = TRUE;

  if(file_check_directory(file_directory_path(), FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
		if(file_check_directory(_op_video_videos_path(), FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {

			$failed = FALSE;

			if(!file_check_directory(_op_video_temp_path(), FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS))
				$failed = TRUE;

			if(!file_check_directory(_op_video_original_path(), FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS))
				$failed = TRUE;

			if(!file_check_directory(_op_video_transcoded_path(), FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS))
				$failed = TRUE;

			if(!file_check_directory(_op_video_image_cache_path(), FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS))
				$failed = TRUE;
		}
	}
	
  if(!file_check_directory(file_directory_temp(), FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS))
  	$failed = TRUE;

	if($failed && user_access('administer video content')) {
		$file_system = l('file system', 'admin/settings/file-system');
		$msg = "Unable to configure video storage.<br />\n";
		$msg .= "Please ensure there are no errors on the $file_system page.";
		drupal_set_message($msg, 'error');
	}

	if(variable_get('op_video_dirs_configured', FALSE)) {
		if($failed) {
			variable_del('op_video_dirs_configured');
			cache_clear_all();
		}
	}
	else {
		if(!$failed) {
			variable_set('op_video_dirs_configured', TRUE);
			cache_clear_all();
		}
	}

	return !$failed;
}


function _op_video_delete_dirs() {

	if(!@rmdir(_op_video_temp_path()))
		$failed = TRUE;

	if(!@rmdir(_op_video_original_path()))
		$failed = TRUE;

	if(!@rmdir(_op_video_transcoded_path()))
		$failed = TRUE;

	if(!@rmdir(_op_video_image_cache_path()))
		$failed = TRUE;

	if(!$failed)
		@rmdir(_op_video_videos_path());
}


function _op_video_insert_file($file, $keepme = NULL) {
	
	if($file_id = _op_video_get_file_id($file->filepath)) {
		_op_video_update_file($file_id, $file, $keepme);
		return $file_id;
	}
	
	$fields = array(
		'filename' => "'%s'",
		'filepath' => "'%s'",
		'filemime' => "'%s'",
		'filesize' => "%d",
		'obtained' => "%d"
	);
	
	$data = array(
		'filename' => $file->filename,
		'filepath' => trim($file->filepath),
		'filemime' => $file->filemime,
		'filesize' => $file->filesize,
		'obtained' => time()
	);
	
	if($keepme) {
		$fields['keepme'] = "%d";
		$data['keepme'] = 1;
	}
	
	return _op_video_db_insert('op_video_files', $fields, $data);
}


function _op_video_update_file($file_id, $file, $keepme = 0) {

	$fields = array(
		'filename' => "'%s'",
		'filepath' => "'%s'",
		'filemime' => "'%s'",
		'filesize' => "%d",
		'keepme' => "%d",
		'dirty' => "%s",
		'obtained' => "%d",
		'deleted' => "%d"
	);
	
	$data = array(
		'filename' => $file->filename,
		'filepath' => trim($file->filepath),
		'filemime' => $file->filemime,
		'filesize' => $file->filesize,
		'keepme' => $keepme,
		'dirty' => 0,
		'obtained' => time(),
		'deleted' => 0
	);
	
	return _op_video_db_update('op_video_files', 'file_id', $file_id, $fields, $data);
}


function _op_video_insert_filepath($filepath, $keepme = FALSE) {

	if(!function_exists('mime_content_type'))
	{
		function mime_content_type($f)
		{
			return trim(exec('file -bi ' . escapeshellarg($f)));
		}
	}

	$fileinfo = @stat($filepath);
	
	if(!$fileinfo)
		return;

	$file->filename = basename($filepath);
	$file->filepath = $filepath;
	$file->filemime = mime_content_type($filepath);
	$file->filesize = $fileinfo['size'];

	return _op_video_insert_file($file, $keepme);
}


function _op_video_fix_uploads() {
	foreach($_FILES as $source_key => $source_value)
		foreach($source_value as $key => $value)
			$_FILES['files'][$key][$source_key] = $value;
}


function _op_video_save_upload($source, $dest, $replace = FILE_EXISTS_RENAME) {

	switch($dest) {
		case 'original':
			$dest = _op_video_original_path();
			break;

		case 'transcoded':
			$dest = _op_video_transcoded_path();
			break;

		default:
			return;
	}
	
	$file = file_check_upload($source);

	if(!$file)
		return;
		
	$file = drupal_clone($file);	// stop nasty prob with upload module on PHP 5

	if(!file_move($file, $dest, $replace)) {
		file_delete($file->filepath);
		return;
	}

	unset($_SESSION['file_uploads'][is_object($source) ? $source->source : $source]);

	return _op_video_insert_file($file);
}


function _op_video_get_file_info($file_id) {

	if(!$file_id)
		return;

	$file = db_fetch_object(db_query("
		SELECT *
		FROM {op_video_files}
		WHERE file_id = %d
	",
		$file_id
	));

	return $file;
}


function _op_video_get_file_id($filepath) {

	$file_id = db_result(db_query("
		SELECT file_id
		FROM {op_video_files}
		WHERE filepath = '%s' AND
			deleted = NULL
	",
		trim($filepath)
	));

	return $file_id;
}


function _op_video_dirty_file($file_id) {

	db_query("
		UPDATE {op_video_files}
		SET dirty = 1
		WHERE file_id = %d
	",
		$file_id
	);
}


function _op_video_is_file_dirty($file) {
	
	if(!is_object($file))
		$file = _op_video_get_file_info($file);

	if(!$file)
		return TRUE;
		
	return $file->dirty;
}


function _op_video_does_file_exist($file) {
	
	if(!is_object($file))
		$file = _op_video_get_file_info($file);

	if(!$file)
		return FALSE;
		
	return !$file->deleted;
}


function _op_video_format_bytesize($bytesize, $decimals = 0) {

	if($bytesize < 1024) {
		$divisor = 1;
		$unit = 'bytes';
	}
	else if($bytesize < 1024 * 1024) {
		$divisor = 1024;
		$unit = 'KB';
	}
	else if($bytesize < 1024 * 1024 * 1024) {
		$divisor = 1024 * 1024;
		$unit = 'MB';
	}
	else {
		$divisor = 1024 * 1024 * 1024;
		$unit = 'GB';
	}
	
	return number_format($bytesize / $divisor, $decimals) . ' ' . $unit;
}


function _op_video_upload_max_size() {
	return _op_video_format_bytesize(file_upload_max_size());
}


function _op_video_delete_file($file) {
	
	if(!is_object($file))	
		$file = _op_video_get_file_info($file);

	if(!$file || $file->keepme || $file->deleted)
		return FALSE;

	file_delete($file->filepath);

	db_query("
		UPDATE {op_video_files}
		SET deleted = %d
		WHERE file_id = %d
	",
		time(),
		$file->file_id
	);

	return TRUE;
}


function _op_video_delete_file_row($file) {
	
	if(!is_object($file))	
		$file = _op_video_get_file_info($file);

	if(!$file)
		return FALSE;

	if(!$file->keepme && !$file->deleted)
		file_delete($file->filepath);

	db_query("
		DELETE FROM {op_video_files}
		WHERE file_id = %d
	",
		$file->file_id
	);

	return TRUE;
}

