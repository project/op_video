<?php

// Copyright 2007 Jonathan Brown

define("OP_VIDEO_DEFAULT_TRANSCODER", 'ffmpeg');

define("OP_VIDEO_EXEC_MODE_NO_ECHO", 0);
define("OP_VIDEO_EXEC_MODE_ECHO", 1);
define("OP_VIDEO_EXEC_MODE_ECHO_AND_STORE", 2);


_op_video_require('db.inc');
_op_video_require('file.inc');
_op_video_require('image_cache.inc');
_op_video_require('preview_image.inc');
_op_video_require('player.inc');
_op_video_require('transcoder.inc');
_op_video_require('views.inc');
_op_video_require('field.inc');


function _op_video_require($filename) {
	$path = drupal_get_path('module', 'op_video');
	require_once './' . $path . '/' . $filename;
}


function op_video_menu($may_cache) {

	if($may_cache) {
		$items[] = array(
			'path' => 'admin/settings/op_video',
			'title' => t('OpenPackage Video'),
			'description' => t('OpenPackage Video settings.'),
			'callback' => 'drupal_get_form',
			'callback arguments' => array('_op_video_admin_settings'),
			'access' => user_access('administer video content'),
			'type' => MENU_NORMAL_ITEM
		);

		$items[] = array(
			'path' => 'op_video/callback',
			'callback' => '_op_video_callback',
			'access' => TRUE,
			'type' => MENU_CALLBACK
		);

		$items[] = array(
			'path' => 'op_video/incoming',
			'callback' => '_op_video_incoming',
			'access' => TRUE,
			'type' => MENU_CALLBACK
		);

    $items[] = array(
      'path' => 'op_video/autocomplete', 
      'title' => t('File autocomplete'),
      'callback' => '_op_video_file_autocomplete', 
      'access' => TRUE, 
      'type' => MENU_CALLBACK
    );
	}
	else {
		$items[] = array(
			'path' => 'op_video/' . arg(1) . '/embed',
			'callback' => '_op_video_embed',
			'callback arguments' => arg(1),
			'access' => TRUE,
			'type' => MENU_CALLBACK
		);
		$items[] = array(
			'path' => 'op_video/download/' . arg(2),
			'callback' => '_op_video_download',
			'callback arguments' => arg(2),
			'access' => TRUE,
			'type' => MENU_CALLBACK
		);
		$items[] = array(
			'path' => 'op_video/download/' . arg(2) . '/' . arg(3),
			'callback' => '_op_video_download',
			'callback arguments' => arg(2),
			'access' => TRUE,
			'type' => MENU_CALLBACK
		);
		
	  if(arg(0) == 'node' && arg(1) != 0) {
	  	$video = _op_video_get_first_video(node_load(arg(1)));
	  	if($video) {
				$items[] = array(
					'path' => 'node/' . arg(1) . '/embed', 
					'title' => 'Embed',
			    'type' => MENU_LOCAL_TASK,
			    'callback' => '_op_video_embed_tab',
					'callback arguments' => $video->video_id
				);
	  	}
	  }

	}
	
	return $items;
}


function op_video_op_video_settings_form() {

	$description = 'Transcoding of videos to Flash format can either occur locally using FFmpeg (if available) or ';
	$description .= 'at <a href="http://openpackage.biz/">OpenPackage.biz</a>.<br />To transcode at OpenPackage.biz you must create an account and ';
	$description .= 'purchase transcoding credit.';

	$form['op_video_transcoder'] = array(
		'#type' => 'radios',
		'#title' => t('Transcoder'),
		'#default_value' => variable_get('op_video_transcoder', OP_VIDEO_DEFAULT_TRANSCODER),
		'#options' => array(
			'ffmpeg' => 'Local FFmpeg installation', 
			'openpackage.biz' => 'OpenPackage.biz'
		),
    '#required' => TRUE,
    '#description' => $description
	);
	
	$form['ffmpeg'] = _op_video_ffmpeg_settings_fieldset();
	$form['openpackage'] = _op_video_openpackage_settings_fieldset();
	
  return $form;
}


function _op_video_admin_settings() {

	_op_video_create_dirs();
	$items = module_invoke_all('op_video_settings_form');
	return system_settings_form($items);
}


function _op_video_admin_settings_validate($form_id, $form_values) {

	module_invoke_all('op_video_settings_form_validate', $form_values);
}


function _op_video_admin_settings_submit($form_id, $form_values) {

	_op_video_openpackage_settings_form_submit($form_values);

	if(function_exists('_op_video_node_settings_form_submit'))
		_op_video_node_settings_form_submit($form_values);

	system_settings_form_submit($form_id, $form_values);
}


function _op_video_create_video($source_file_id, $dirty_params_id, $preview_image = 'third', $custom_image_file_id = NULL) {

	$fields = array(
		'secret' => "'%s'",
		'source_file_id' => "%d",
		'dirty_params_id' => "%d",
		'preview_image' => "'%s'"
	);

	$data = array(
		'secret' => md5(user_password()),
		'source_file_id' => $source_file_id,
		'dirty_params_id' => $dirty_params_id,
		'preview_image' => $preview_image
	);
	
	if($custom_image_file_id != NULL) {
		$fields['custom_image_file_id'] = "%d";
		$data['custom_image_file_id'] = $custom_image_file_id;
	}
		
	return _op_video_db_insert('op_videos', $fields, $data);
}


function _op_video_load($video) {

	if(!is_object($video)) {
		$video = db_fetch_object(db_query("
		 	SELECT *
			FROM {op_videos}
			WHERE video_id = %d
		",
			$video
		));
	}

	if(!$video)
		return;

	$video->source_file = _op_video_get_file_info($video->source_file_id);
	$video->flv_file = _op_video_get_file_info($video->flv_file_id);
	$video->start_image_file = _op_video_get_file_info($video->start_image_file_id);
	$video->third_image_file = _op_video_get_file_info($video->third_image_file_id);
	$video->custom_image_file = _op_video_get_file_info($video->custom_image_file_id);

	$video->current_params = _op_video_transcoding_params_load($video->current_params_id);
	$video->dirty_params = _op_video_transcoding_params_load($video->dirty_params_id);
	return $video;
}


function _op_video_update_custom_image($video, $file_id) {
	
	db_query("
  	UPDATE {op_videos}
		SET custom_image_file_id = %d
		WHERE video_id = %d
	",
		$file_id,
		$video->video_id
	);

	if($video->custom_image_file_id)
		_op_video_delete_file_row($video->custom_image_file);

	_op_video_purge_image_cache($video->video_id);
}


function _op_video_retranscode_video($video_id) {

	db_query("
  	UPDATE {op_videos}
		SET	secret = '%s',
			status = 'dirty', 
			upload_failed = 0,
			uploaded = 0
		WHERE video_id = %d
	",
		md5(user_password()),
		$video_id
	);

	drupal_set_message('The video will be retranscoded.');
}


function _op_video_delete($video) {

	if(!is_object($video))
		$video = _op_video_load($video);
		
	if(!$video)
		return;

	db_query("
		DELETE FROM {op_videos}
		WHERE video_id = %d
	",
		$video->video_id
	);

	_op_video_delete_file_row($video->source_file);
	_op_video_delete_file_row($video->flv_file);
	_op_video_delete_file_row($video->start_image_file_id);
	_op_video_delete_file_row($video->third_image_file_id);
	_op_video_delete_file_row($video->custom_image_file_id);

	_op_video_transcoding_params_delete($video->current_params_id);

	db_query("
		DELETE FROM {op_video_stats}
		WHERE video_id = %d
	",
		$video->video_id
	);
	
	_op_video_purge_image_cache($video->video_id);
}


function _op_video_check_video_id($video_id) {
	
	return db_result(db_query("
		SELECT COUNT(*)
		FROM {op_videos}
		WHERE video_id = %d
	",
		$video_id
	));
}


function _op_video_render_preview_image_link($video, $nid, $width = NULL, $height = NULL) {
	$thumb = _op_video_render_preview_image($video, $width, $height);
	$link = l($thumb, 'node/' . $nid, NULL, NULL, NULL, FALSE, TRUE);
	return theme('op_video_preview_image', $link);
}


function _op_video_set_render_settings($settings = NULL) {
	global $_op_video_render_settings;

	$_op_video_render_settings = $settings;
}


function _op_video_get_render_setting($setting, $default) {
	global $_op_video_render_settings;
		
	return isset($_op_video_render_settings[$setting]) ? $_op_video_render_settings[$setting] : $default;
}


function _op_video_render_video($video, $formatter, $nid, $autostart = 0) {

	if($video->flv_file) {
		switch($formatter) {
			case 'default':
				$output = _op_video_render_inline($video, $autostart);
				break;

			case 'preview-image-layer':
				$output = _op_video_render_preview_image_layer($video);
				break;

			case 'preview-image-link':
				$output = _op_video_render_preview_image_link($video, $nid);
				break;
		}
	}
	else if(variable_get('op_video_transcoder', OP_VIDEO_DEFAULT_TRANSCODER) == 'openpackage.biz' && 
		user_access('administer video content'))
	{
		switch($video->error) {
		
			case 'none':
			case 'connection error':
				switch($video->status) {
				
					case 'dirty':
						$output = '<p>The video is awaiting upload to openpackage.biz.</p>';
						break;

					case 'uploaded':
						$output = '<p>The video has been uploaded to openpackage.biz for transcoding.</p>';
						break;
				}
				break;

			case 'invalid credentials':
				$output = '<p>Your credentials are invalid. Please enter your correct openpackage.biz ' . l('credentials', 'admin/settings/op_video') . '.</p>';
				break;

			case 'insufficient credit':
				$output = '<p>There was insufficient credit at the time of uploading to transcode this video. ';
				$output .= 'Please purchase more credit at ' . l('openpackage.biz', 'http://openpackage.biz/') . '.</p>';
				break;

			case 'transcoding failed':
				$output = '<p>Transcoding failed. Please try another video.</p>';
				break;
		}
	}
	else {
		switch($video->status) {
			case 'dirty':
			case 'uploaded':
				$output = '<p>Processing video... This may take a few minutes. Please '. l('refresh', $_GET['q']) . '.</p>';
				break;

			case 'idle':
				$output = '<p>Transcoding failed. Please try another video.</p>';
				break;
		}
	}
	
	return $output;
}


function rsi($parts, $glue = '') {
	
	if(!is_array($parts))
		return $parts;

	foreach($parts as $key => $part) {
		$parts[$key] = rsi($part, $glue);
	}

	return implode($glue, $parts);
}


function _op_video_exec($command, $mode, &$output, &$result) {

	$command = rsi(array('time nice -n 19', $command, '2>&1'), ' ');

	switch($mode) {
	
		case OP_VIDEO_EXEC_MODE_NO_ECHO:
			$output[] = $command;
			exec($command, $output, $result);
			break;
	
		case OP_VIDEO_EXEC_MODE_ECHO:
			echo $command . "\n";
			system($command, $result);
			break;
	
		case OP_VIDEO_EXEC_MODE_ECHO_AND_STORE:
			echo $command . "\n";
			exec($command, $output, $result);
			break;
	}
}


function op_video_cron() {

	if(variable_get('op_video_transcoder', OP_VIDEO_DEFAULT_TRANSCODER) == 'openpackage.biz' &&
		variable_get('op_video_openpackage_return_method', OP_VIDEO_DEFAULT_OPENPACKAGE_RETURN_METHOD) == 'poll')
		_op_video_retrieve_videos();

	_op_video_transcode_videos();
}


function _op_video_log($type, $video_id) {

	switch($type) {
	
		case 'view':
			$field = 'views';
			break;
			
		case 'view completed':
			$field = 'views_completed';
			break;
			
		case 'download':
			$field = 'downloads';
			break;
	}

	db_query("
		UPDATE {op_videos}
		SET $field = $field + 1
		WHERE video_id = $video_id
	");

	if(!db_affected_rows())
		return;


	global $user;
	$sid = session_id();
	$ip_addr = $_SERVER['REMOTE_ADDR'];
	$time = time();

	db_query("
		INSERT INTO {op_video_stats}
		SET type = '$type',
			video_id = $video_id,
			uid = $user->uid,
			sid = '$sid',
			ip_addr = INET_ATON('$ip_addr'),
			timestamp = $time
	");
}


function _op_video_callback() {
	switch($_POST['state']) {
	
		case 'start':
			$type = 'view';
			break;
			
		case 'stop':
			$type = 'view completed';
			break;
			
		default:
			return;
	}

	_op_video_log($type, $_POST['id']);
}


function _op_video_stat_count($type, $duration, $video_id) {

	$count = (int)db_result(db_query("
		SELECT COUNT(*)
		FROM {op_video_stats}
		WHERE type = '%s' AND
			video_id = %d AND
			timestamp > %d
		GROUP BY video_id
	",
		$type,
		$video_id,
		time() - $duration
	));
	
	return $count;
}


function _op_video_download_path($video) {
	global $base_url;
	
	$path = 'op_video/download/' . $video->video_id;

	if(!variable_get('clean_url', '0'))
		return $path;

	return $base_url . '/' . $path . '/' . rawurlencode($video->source_file->filename); 
}


function _op_video_download($video_id) {
	$video = _op_video_load($video_id);
	
	if(!$video->source_file || $video->source_file->deleted) {
		drupal_not_found();
		exit();
	}
	
	_op_video_log('download', $video->video_id);

	$headers[] = 'HTTP/1.0 200 OK';
	$headers[] = 'Content-Disposition: attachment; filename="' . $video->source_file->filename . '"';
	$headers[] = 'Content-Type: ' . $video->source_file->filemime;
	$headers[] = 'Content-Length: ' . $video->source_file->filesize;
  file_transfer($video->source_file->filepath, $headers);
}


function _op_video_file_autocomplete() {

  $args = func_get_args();
	$string = implode('/', $args);

	if($string) {
	  $files = file_scan_directory(_op_video_original_path(), '.*');
	  foreach ($files as $f) {
	//    $trimmed = trim(str_replace(file_directory_path() .'/', '', $f->filename));
	    $trimmed = trim($f->filename);
	    if(strpos($trimmed, $string) !== FALSE) {
	      $matches[$trimmed] = check_plain($trimmed);
	    }
	  }
	}
	
  print drupal_to_js($matches);
  exit();
}


function _op_video_generate_embed_code($video_id) {

	$video = _op_video_load($video_id);

	$url = url('op_video/' . $video_id . '/embed', NULL, NULL, TRUE);
	
	$code = "<iframe src=\"" . $url . "\" width=\"400\" height=\"300\" frameborder=\"0\" scrolling=\"no\"></iframe>";
	
	return htmlspecialchars($code);
}


function _op_video_embed_tab($video_id) {

	$node = node_load(arg(1));
  drupal_set_title(check_plain($node->title));


	$output .= "<code>\n";
	$output .= _op_video_generate_embed_code($video_id);
	$output .= "</code>\n";

	return $output;
}


function _op_video_embed($video_id) {
	
	$video = _op_video_load($video_id);
	
	$settings['player_width'] = '400';
	$settings['player_height'] = '300';

	$settings['preview_image_user_selectable'] = TRUE;

	$settings['controlbar'] = FALSE;

	$settings['showicons'] = TRUE;
	$settings['showdigits'] = TRUE;
	$settings['showfsbutton'] = TRUE;
	$settings['repeat'] = FALSE;
	$settings['showvolume'] = TRUE;
	$settings['autostart'] = FALSE;
	$settings['showdownload'] = FALSE;

	_op_video_set_render_settings($settings);

	$player = _op_video_render_inline($video, 0);
	
	$ufo_path = base_path() . drupal_get_path('module', 'op_video') . '/ufo.js';
	
  $output  = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
  $output .= "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"$language\" xml:lang=\"$language\">\n";
  $output .= "<head>\n";
  $output .= "<script type=\"text/javascript\" src=\"" . $ufo_path . "\"></script>\n";
  $output .= "</head>";
  $output .= "<body>\n";
	$output .= $player;
  $output .= " </body>\n";
  $output .= "</html>\n";
  
  echo $output;
}


function _op_video_jpeg_quality_change() {

	_op_video_purge_image_cache();
}


function op_video_form_alter($form_id, &$form) {

	switch($form_id) {
		case 'system_image_toolkit_settings':
			$form['#submit']['_op_video_jpeg_quality_change'] = array();
			break;
	}
}

function theme_op_video_preview_image($thumb) {
	$output .= "<center>\n";
	$output .= $thumb . "\n";
	$output .= "</center>\n";
	return $output;
}


function theme_op_video_player($player) {
	$output .= "<center>\n";
	$output .= $player . "\n";
	$output .= "</center>\n";
	return $output;
}

