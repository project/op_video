<?php

// Copyright 2007 Jonathan Brown

define("OP_VIDEO_OPENPACKAGE_URL", 'http://openpackage.biz');

define("OP_VIDEO_DEFAULT_OPENPACKAGE_RETURN_METHOD", 'poll');
define("OP_VIDEO_OPENPACKAGE_PROTO_VERSION", 3);

define("HTTP_TIMEOUT", 4);


function _op_video_openpackage_settings_fieldset() {

	$fieldset = array(
		'#type' => 'fieldset',
		'#title' => t('OpenPackage.biz credentials'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE
	);

  $fieldset['op_video_openpackage_name'] = array(
		'#type' => 'textfield',
    '#title' => t('Username'),
    '#size' => 15,
    '#maxlength' => USERNAME_MAX_LENGTH,
		'#default_value' => variable_get('op_video_openpackage_name', '')
  );

  $fieldset['op_video_openpackage_hash'] = array(
		'#type' => 'password',
    '#title' => t('Password'),
    '#maxlength' => 60,
    '#size' => 15
  );

	$curl = function_exists('curl_version');

	if(!$curl)
		variable_set('op_video_openpackage_return_method', 'direct');

	$fieldset['op_video_openpackage_return_method'] = array(
		'#type' => 'radios',
		'#title' => t('Return method'),
		'#options' => array(
			'direct' => 'Direct return',
			'poll' => 'Periodic check'
		),
		'#default_value' => 
			variable_get('op_video_openpackage_return_method', OP_VIDEO_DEFAULT_OPENPACKAGE_RETURN_METHOD),
    '#required' => TRUE,
		'#description' => 'Direct return is the optimal setting, but may not work if a firewall is blocking port 80.'
	);

  if(!$curl) {
		$fieldset['op_video_openpackage_return_method']['#disabled'] = TRUE;
		$fieldset['op_video_openpackage_return_method']['#description'] .=
			'<br />Periodic check is not available because <a href="http://php.net/curl">cURL support</a> is not enabled.';
	}

  return $fieldset;
}


function _op_video_reattempt_invalid_credentials() {

	db_query("
  	UPDATE {op_videos}
		SET	status = 'dirty',
			error = 'none',
			upload_failed = 0
  	WHERE error = 'invalid credentials'
	");

	if(db_affected_rows()) {
		$msg = 'Reattempting uploads that failed due to invalid credentials.';
		drupal_set_message($msg);
		watchdog('op_video', $msg);
	}
}


function _op_video_openpackage_settings_form_submit(&$form_values) {

	switch($form_values['op']) {

		case t('Save configuration'):
			$form_values['op_video_openpackage_hash'] = trim($form_values['op_video_openpackage_hash']);

			if($form_values['op_video_openpackage_hash'] != '') {
				$form_values['op_video_openpackage_hash'] = md5($form_values['op_video_openpackage_hash']);
				_op_video_reattempt_invalid_credentials();
			}
			else {
				unset($form_values['op_video_openpackage_hash']);
			}

			break;
	}
}


function _op_video_upload_video($video) {
	
	$fields['proto_ver'] = OP_VIDEO_OPENPACKAGE_PROTO_VERSION;

	$fields['name'] = variable_get('op_video_openpackage_name', '');
	$fields['hash'] = variable_get('op_video_openpackage_hash', '');

	$fields['site_url'] = url('', NULL, NULL, TRUE);
	$fields['return_method'] = variable_get('op_video_openpackage_return_method', 
		OP_VIDEO_DEFAULT_OPENPACKAGE_RETURN_METHOD);
	$fields['video_id'] = $video->video_id;
	$fields['secret'] = $video->secret;

	unset($video->dirty_params['keep_source_file']);
	$fields['params'] = serialize($video->dirty_params);

	$video->source_file->field = 'video';

	$url = variable_get('op_video_openpackage_url', OP_VIDEO_OPENPACKAGE_URL) . '/?q=incoming';
	return _op_video_multipart_http_request($url, $fields, array($video->source_file));
}


function _op_video_transcode_openpackage($video) {

/*
	if($video->upload_failed && 
		$video->upload_failed > time() - 10 * 60)
	{
		return;
	}
*/

	$result = _op_video_upload_video($video);
	
	if(!$result) {

		db_query("
			UPDATE {op_videos}
			SET status = 'idle',
				error = 'file not found'
			WHERE video_id = %d
		",
			$video->video_id
		);
	}
	else {
		switch($result->code) {

			case 200:
				db_query("
					UPDATE {op_videos}
					SET status = 'uploaded',
						error = 'none',
						uploaded = %d
					WHERE video_id = %d
				",
					time(),
					$video->video_id
				);

				watchdog('op_video', 'Video successfully uploaded. video_id: ' . $video->video_id);
				break;

			case 402:
				db_query("
					UPDATE {op_videos}
					SET status = 'uploaded',
						error = 'insufficient credit',
						uploaded = %d
					WHERE video_id = %d
				",
					time(),
					$video->video_id
				);

				watchdog('op_video', 'Video uploaded. Insufficient credit. video_id: ' . $video->video_id, WATCHDOG_WARNING);
				break;

			case 403:
				db_query("
					UPDATE {op_videos}
					SET status = 'idle',
						error = 'invalid credentials'
					WHERE video_id = %d
				",
					$video->video_id
				);

				watchdog('op_video', 'Video upload failed. Invalid credentials. video_id: ' . $video->video_id, WATCHDOG_WARNING);
				break;

			case 406:
				db_query("
					UPDATE {op_videos}
					SET status = 'idle',
						error = 'transcoding failed'
					WHERE video_id = %d
				",
					$video->video_id
				);

//				watchdog('op_video', 'Video upload failed. Invalid credentials. video_id: ' . $video->video_id, WATCHDOG_WARNING);
				break;

			default:
				db_query("
					UPDATE {op_videos}
					SET status = 'dirty',
						error = 'connection error',
						upload_failed = %d
					WHERE video_id = %d
				",
					time(),
					$video->video_id
				);

				watchdog('op_video', 'Video upload failed - will reattempt in 10 min. Connection error. video_id: ' . $video->video_id, WATCHDOG_WARNING);
				break;
		}
	}
}


function _op_video_curl_implode_fields(&$fields) {
	
	foreach($fields as $key => $value) {
		$fields[$key] = $key . '=' . $value;
	}
	
	$fields = implode('&', $fields);
}


function _op_video_curl_post($url, $fields) {

	$ch = curl_init($url);
	_op_video_curl_implode_fields($fields);

	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

	$result = curl_exec($ch);
	curl_close($ch);
	
	return $result;
}


function _op_video_curl_post_get_file($url, $fields, $filepath) {

	$ch = curl_init($url);
	_op_video_curl_implode_fields($fields);
	
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

	$fp = fopen($filepath, "w");
	curl_setopt($ch, CURLOPT_FILE, $fp);

	curl_exec($ch);
	fclose ($fp);
	$result = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	
	return $result;
}


function _op_video_openpackage_get_file($video, $file) {
	$url = variable_get('op_video_openpackage_url', OP_VIDEO_OPENPACKAGE_URL) . '/?q=get';

	$fields['name'] = variable_get('op_video_openpackage_name', '');
	$fields['hash'] = variable_get('op_video_openpackage_hash', '');
	$fields['site_url'] = url('', NULL, NULL, TRUE);
	$fields['video_id'] = $video->video_id;
	$fields['file'] = $file;

	switch($file) {

		case 'flv':
			$ext = '.flv';
			break;

		case 'raw_image_1':
			$ext = '_raw_image_1.png';
			break;

		case 'raw_image_2':
			$ext = '_raw_image_2.png';
			break;
	}

	$filepath = _op_video_temp_path() . '/' . $video->video_id . $ext;
	
	$result = _op_video_curl_post_get_file($url, $fields, $filepath);
	
	if($result == 200) {
		file_move($filepath, _op_video_transcoded_path(), FILE_EXISTS_REPLACE);
		return _op_video_insert_filepath($filepath);
	}
	else {
		file_delete($filepath);
		return -$result;
	}
}


function _op_video_openpackage_get_info($video) {
	$url = variable_get('op_video_openpackage_url', OP_VIDEO_OPENPACKAGE_URL) . '/?q=get';

	$fields['name'] = variable_get('op_video_openpackage_name', '');
	$fields['hash'] = variable_get('op_video_openpackage_hash', '');
	$fields['site_url'] = url('', NULL, NULL, TRUE);
	$fields['video_id'] = $video->video_id;
	$fields['info'] = TRUE;

	return unserialize(_op_video_curl_post($url, $fields));
}


function _op_video_retrieve_videos() {

	$videos_result = db_query("
  	SELECT *
  	FROM {op_videos}
  	WHERE status = 'uploaded'
		ORDER BY video_id
	");

	while($video = db_fetch_object($videos_result)) {
	
		$flv_file_id = _op_video_openpackage_get_file($video, 'flv');
		
		if($flv_file_id > 0) {
			$raw_image_1_file_id = _op_video_openpackage_get_file($video, 'raw_image_1');
			$raw_image_2_file_id = _op_video_openpackage_get_file($video, 'raw_image_2');
			$info = _op_video_openpackage_get_info($video);

			$params_id = _op_video_transcoding_params_insert($info['params']);

			db_query("
				UPDATE {op_videos}
				SET duration = %d,
					flv_file_id = %d,
					start_image_file_id = %d,
					third_image_file_id = %d,
					current_params_id = %d,
					status = 'idle',
					error = 'none'
				WHERE video_id = %d
			",
				$info['duration'],
				$flv_file_id,
				$raw_image_1_file_id,
				$raw_image_2_file_id,
				$params_id,
				$video->video_id
			);

			_op_video_transcoding_completed($video->video_id);
			watchdog('op_video', 'Video returned. video_id: ' . $video_id);
		}
		else {
		
			switch(-$flv_file_id) {

				case 204:		// not transcoded yet
					break;
			
				case 403:
					db_query("
						UPDATE {op_videos}
						SET error = 'invalid credentials'
						WHERE video_id = %d
					",
						$video->video_id
					);
					break;
			
				case 406:
					db_query("
						UPDATE {op_videos}
						SET status = 'idle',
							error = 'transcoding failed'
						WHERE video_id = %d
					",
						$video->video_id
					);
					break;
			}
		}
	}
}


function _op_video_incoming() {

	watchdog('op_video', var_export($_POST, TRUE));

	$video_id = $_POST['video_id'];

	$video = db_fetch_object(db_query("
		SELECT *
		FROM {op_videos}
		WHERE video_id = %d AND
			secret = '%s'
	",
		$video_id,
		$_POST['secret']
	));

	if(!$video) {
		watchdog('op_video', 'Returned video invalid. $_POST: ' . var_export($_POST, TRUE), WATCHDOG_WARNING);
		return;
	}

	if($_POST['error']) {

		db_query("
			UPDATE {op_videos}
			SET status = 'idle',
				error = 'transcoding failed'
			WHERE video_id = %d
		",
			$video_id
		);

		watchdog('op_video', 'Error returned. video_id: ' . $video_id);
		return;
	}

	_op_video_fix_uploads();

	$start_image_file_id = _op_video_save_upload('raw_image_1', 'transcoded', FILE_EXISTS_REPLACE);
	$third_image_file_id = _op_video_save_upload('raw_image_2', 'transcoded', FILE_EXISTS_REPLACE);
	$flv_file_id = _op_video_save_upload('flv', 'transcoded', FILE_EXISTS_REPLACE);
	$params_id = _op_video_transcoding_params_insert(unserialize($_POST['params']));

	db_query("
		UPDATE {op_videos}
		SET duration = %d,
			flv_file_id = %d,
			start_image_file_id = %d,
			third_image_file_id = %d,
			current_params_id = %d,
			status = 'idle',
			error = 'none'
		WHERE video_id = %d
	",
		$_POST['duration'],
		$flv_file_id,
		$start_image_file_id,
		$third_image_file_id,
		$params_id,
		$video_id
	);

	_op_video_transcoding_completed($video->video_id);
	watchdog('op_video', 'Video returned. video_id: ' . $video_id);
}


function _op_video_multipart_http_request($url, $fields = array(), $files = array(), $retry = 3) {
  $result = new stdClass();
  
  // Parse the URL, and make sure we can handle the schema.
  $uri = parse_url($url);
  switch ($uri['scheme']) {
    case 'http':
      $port = isset($uri['port']) ? $uri['port'] : 80;
      $host = $uri['host'] . ($port != 80 ? ':'. $port : '');
      $fp = @fsockopen($uri['host'], $port, $errno, $errstr, HTTP_TIMEOUT);
      break;
    case 'https':
      // Note: Only works for PHP 4.3 compiled with OpenSSL.
      $port = isset($uri['port']) ? $uri['port'] : 443;
      $host = $uri['host'] . ($port != 443 ? ':'. $port : '');
      $fp = @fsockopen('ssl://'. $uri['host'], $port, $errno, $errstr, HTTP_TIMEOUT);
      break;
    default:
      $result->error = 'invalid schema '. $uri['scheme'];
      return $result;
  }

  // Make sure the socket opened properly.
  if (!$fp) {
    $result->error = trim($errno .' '. $errstr);
    return $result;
  }

	@stream_set_timeout($fp, HTTP_TIMEOUT);

  // Construct the path to act on.
  $path = isset($uri['path']) ? $uri['path'] : '/';
  if (isset($uri['query'])) {
    $path .= '?'. $uri['query'];
  }

	$boundary = "---------------------------" . substr(md5(rand(0, 32000)), 0, 10);

	foreach($fields as $field_name => $field_data) {

		$data .= '--' . $boundary . "\r\n";
		$data .= 'content-disposition: form-data; name="' . $field_name . '"';
		$data .= "\r\n\r\n";
		$data .= $field_data;
		$data .= "\r\n";
	}

	$size += strlen($data);

	foreach($files as $key => $file) {

		$files[$key]->data = '--' . $boundary . "\r\n";
		$files[$key]->data .= 'content-disposition: form-data; name="' . $file->field . '"; filename="' . $file->filename . '"';
		$files[$key]->data .= "\r\n";
		$files[$key]->data .= 'Content-Type: ' . $file->filemime;
		$files[$key]->data .= "\r\n";
		$files[$key]->data .= 'Content-Transfer-Encoding: binary';
		$files[$key]->data .= "\r\n\r\n";

		$size += strlen($files[$key]->data) + $file->filesize + 2;
	}

	$size += strlen($boundary) + 6;

  // Create HTTP request.
  $defaults = array(
    // RFC 2616: "non-standard ports MUST, default ports MAY be included".
    // We don't add the port to prevent from breaking rewrite rules checking
    // the host that do not take into account the port number.
    'Host' => "Host: $host",
		'Content-type' => "Content-type: multipart/form-data, boundary=$boundary",
//    'User-Agent' => 'User-Agent: Drupal (+http://drupal.org/)',
    'Content-Length' => 'Content-Length: '. $size
  );
  
  $request = 'POST '. $path ." HTTP/1.0\r\n";
  $request .= implode("\r\n", $defaults);
  $request .= "\r\n\r\n";
  $request .= $data;
  $result->request = $request;

  fwrite($fp, $request);

	foreach($files as $file) {

	  fwrite($fp, $file->data);

		$fr = fopen($file->filepath, 'r');

		if(!$fr){
		  fclose($fp);
			return;		// report failure
		}

		$bytes = 0;
	  while (!feof($fr) && $chunk = fread($fr, 1024)) {
		  $bytes += fwrite($fp, $chunk);
	  }

		fclose($fr);

		if($bytes != $file->filesize) {
		  fclose($fp);
			return;		// report failure
		}

	  fwrite($fp, "\r\n");
	}

  fwrite($fp, "--" . $boundary . "--\r\n");

  // Fetch response.
  $response = '';
  while (!feof($fp) && $chunk = fread($fp, 1024)) {
    $response .= $chunk;
  }
  fclose($fp);

  // Parse response.
  list($split, $result->data) = explode("\r\n\r\n", $response, 2);
  $split = preg_split("/\r\n|\n|\r/", $split);

  list($protocol, $code, $text) = explode(' ', trim(array_shift($split)), 3);
  $result->headers = array();

  // Parse headers.
  while ($line = trim(array_shift($split))) {
    list($header, $value) = explode(':', $line, 2);
    if (isset($result->headers[$header]) && $header == 'Set-Cookie') {
      // RFC 2109: the Set-Cookie response header comprises the token Set-
      // Cookie:, followed by a comma-separated list of one or more cookies.
      $result->headers[$header] .= ',' . trim($value);
    }
    else {
      $result->headers[$header] = trim($value);
    }
  }

  $responses = array(
    100 => 'Continue', 101 => 'Switching Protocols',
    200 => 'OK', 201 => 'Created', 202 => 'Accepted', 203 => 'Non-Authoritative Information', 204 => 'No Content', 205 => 'Reset Content', 206 => 'Partial Content',
    300 => 'Multiple Choices', 301 => 'Moved Permanently', 302 => 'Found', 303 => 'See Other', 304 => 'Not Modified', 305 => 'Use Proxy', 307 => 'Temporary Redirect',
    400 => 'Bad Request', 401 => 'Unauthorized', 402 => 'Payment Required', 403 => 'Forbidden', 404 => 'Not Found', 405 => 'Method Not Allowed', 406 => 'Not Acceptable', 407 => 'Proxy Authentication Required', 408 => 'Request Time-out', 409 => 'Conflict', 410 => 'Gone', 411 => 'Length Required', 412 => 'Precondition Failed', 413 => 'Request Entity Too Large', 414 => 'Request-URI Too Large', 415 => 'Unsupported Media Type', 416 => 'Requested range not satisfiable', 417 => 'Expectation Failed',
    500 => 'Internal Server Error', 501 => 'Not Implemented', 502 => 'Bad Gateway', 503 => 'Service Unavailable', 504 => 'Gateway Time-out', 505 => 'HTTP Version not supported'
  );
  // RFC 2616 states that all unknown HTTP codes must be treated the same as
  // the base code in their class.
  if (!isset($responses[$code])) {
    $code = floor($code / 100) * 100;
  }
  
  switch ($code) {
    case 200: // OK
    case 304: // Not modified
      break;
    case 301: // Moved permanently
    case 302: // Moved temporarily
    case 307: // Moved temporarily
      $location = $result->headers['Location'];

      if ($retry) {
        $result = _op_video_multipart_http_request($result->headers['Location'], $fields, $files, --$retry);
        $result->redirect_code = $result->code;
      }
      $result->redirect_url = $location;

      break;
    default:
      $result->error = $text;
  }
  
//  var_dump($result);

  $result->code = $code;

  return $result;
}

