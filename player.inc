<?php

// Copyright 2007 Jonathan Brown

define("OP_VIDEO_DEFAULT_PLAYER_WIDTH", 320);
define("OP_VIDEO_DEFAULT_PLAYER_HEIGHT", 240);
define("OP_VIDEO_DEFAULT_SHOW_ICONS", 1);
define("OP_VIDEO_DEFAULT_SHOW_CONTROLBAR", 1);
define("OP_VIDEO_DEFAULT_SHOW_DIGITS", 1);
define("OP_VIDEO_DEFAULT_SHOW_FSBUTTON", 1);
define("OP_VIDEO_DEFAULT_SHOWDOWNLOAD", 1);
define("OP_VIDEO_DEFAULT_AUTOSTART", 'false');
define("OP_VIDEO_DEFAULT_AUTOREPEAT", 0);
define("OP_VIDEO_DEFAULT_VOLUME", 80);
define("OP_VIDEO_DEFAULT_SHOWVOLUME", 1);
define("OP_VIDEO_DEFAULT_BUFFER_LENGTH", 5);
define("OP_VIDEO_DEFAULT_BACKCOLOR", 'FFFFFF');
define("OP_VIDEO_DEFAULT_FRONTCOLOR", '000000');
define("OP_VIDEO_DEFAULT_LIGHTCOLOR", '000000');

define("OP_VIDEO_MAX_VOLUME", 100);
define("OP_VIDEO_MAX_BUFFER_LENGTH", 60);


function _op_video_player_settings_fields() {

	return array(
		'player_width',
		'player_height',
		'showicons',
		'controlbar',
		'showdigits',
		'showfsbutton',
		'showdownload',
		'autostart',
		'repeat',
		'volume',
		'showvolume',
		'bufferlength',
		'backcolor',
		'frontcolor',
		'lightcolor'
	);
}


function _op_video_player_settings_fieldset($settings = NULL) {

	$fieldset = array(
		'#type' => 'fieldset',
		'#title' => t('Player'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE
	);

  $fieldset['player_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Video width'),
    '#default_value' => isset($settings['player_width']) ? $settings['player_width'] : OP_VIDEO_DEFAULT_PLAYER_WIDTH,
//    '#description' => t('Show or hide the play and activity icons in the middle of the video.'),
		'#size' => 6,
		'#maxlength' => 4,
		'#required' => TRUE
  );

  $fieldset['player_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Video height'),
    '#default_value' => isset($settings['player_height']) ? $settings['player_height'] : OP_VIDEO_DEFAULT_PLAYER_HEIGHT,
//    '#description' => t('Show or hide the play and activity icons in the middle of the video.'),
		'#size' => 6,
		'#maxlength' => 4,
		'#required' => TRUE
  );

  $fieldset['showicons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display icons'),
    '#default_value' => isset($settings['showicons']) ? $settings['showicons'] : OP_VIDEO_DEFAULT_SHOW_ICONS,
    '#description' => t('Show or hide the play and activity icons in the middle of the video.'),
  );

  $fieldset['controlbar'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display controlbar'),
    '#default_value' => isset($settings['controlbar']) ? $settings['controlbar'] : OP_VIDEO_DEFAULT_SHOW_CONTROLBAR,
    '#description' => t('Show or hide the controlbar under the video (hiding is useful for adverts).'),
  );

  $fieldset['showdigits'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display digits'),
    '#default_value' => isset($settings['showdigits']) ? $settings['showdigits'] : OP_VIDEO_DEFAULT_SHOW_DIGITS,
    '#description' => t("Disable this if you don't want the elapsed / remaining time to display in the controlbar.
			<br />Quite handy to save some space."),
  );

  $fieldset['showfsbutton'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display fullscreen button'),
    '#default_value' => isset($settings['showfsbutton']) ? $settings['showfsbutton'] : OP_VIDEO_DEFAULT_SHOW_FSBUTTON,
    '#description' => t("The fullscreen button will only appear with <a href=\"http://www.adobe.com/products/flash/about/\">Flash</a> 9.0.28 onwards."),
  );

  $fieldset['showdownload'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display download button'),
    '#default_value' => isset($settings['showdownload']) ? $settings['showdownload'] : OP_VIDEO_DEFAULT_SHOWDOWNLOAD,
    '#description' => t("Only works if the source video is still available."),
  );

  $fieldset['autostart'] = array(
    '#type' => 'radios',
    '#title' => t('Autostart'),
    '#default_value' => isset($settings['autostart']) ? $settings['autostart'] : OP_VIDEO_DEFAULT_AUTOSTART,
    '#description' => t("Start playing the video when the page loads (only applies when the node is being viewed as a page)."),
		'#options' => array(
			'true' => 'Yes', 
			'muted' => 'Yes, but muted',
			'false' => 'No'
		),
    '#required' => TRUE,
  );

  $fieldset['repeat'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autorepeat'),
    '#default_value' => isset($settings['repeat']) ? $settings['repeat'] : OP_VIDEO_DEFAULT_AUTOREPEAT,
    '#description' => t("Automatically repeat the video after it has finished playing."),
  );

  $fieldset['volume'] = array(
    '#type' => 'textfield',
    '#title' => t('Volume'),
    '#default_value' => isset($settings['volume']) ? $settings['volume'] : OP_VIDEO_DEFAULT_VOLUME,
		'#size' => 6,
		'#maxlength' => 3, 
		'#attributes' => array('autocomplete' => 'off'),
    '#required' => TRUE,
    '#description' => t('Initial volume level (0 - ') . OP_VIDEO_MAX_VOLUME . ').',
  );

  $fieldset['showvolume'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show volume'),
    '#default_value' => isset($settings['showvolume']) ? $settings['showvolume'] : OP_VIDEO_DEFAULT_SHOWVOLUME
  );

  $fieldset['bufferlength'] = array(
    '#type' => 'textfield',
    '#title' => t('Buffer length'),
    '#default_value' => isset($settings['bufferlength']) ? $settings['bufferlength'] : OP_VIDEO_DEFAULT_BUFFER_LENGTH,
		'#size' => 6,
		'#maxlength' => 2, 
		'#attributes' => array('autocomplete' => 'off'),
    '#required' => TRUE,
    '#description' => t('This sets the number of seconds an FLV should be buffered ahead before the player starts 
			it.<br />Set this smaller for fast connections or short videos.<br />Set this bigger for slow connections.'),
  );

  $fieldset['backcolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Background colour'),
		'#size' => 6,
		'#maxlength' => 6,
		'#default_value' => isset($settings['backcolor']) ? $settings['backcolor'] : OP_VIDEO_DEFAULT_BACKCOLOR
  );

  $fieldset['frontcolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Text / button colour'),
		'#size' => 6,
		'#maxlength' => 6,
		'#default_value' => isset($settings['frontcolor']) ? $settings['frontcolor'] : OP_VIDEO_DEFAULT_FRONTCOLOR
  );

  $fieldset['lightcolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Hover Colour'),
		'#size' => 6,
		'#maxlength' => 6,
		'#default_value' => isset($settings['lightcolor']) ? $settings['lightcolor'] : OP_VIDEO_DEFAULT_LIGHTCOLOR
  );
  
  return $fieldset;
}


function _op_video_player_settings_validate($form_values) {

	settype($form_values['volume'], 'int');
	settype($form_values['bufferlength'], 'int');

	if($form_values['volume'] < 0) {
		form_set_error('volume', t('Volume field must be greater than or equal to 0.'));
	}
	else if($form_values['volume'] > OP_VIDEO_MAX_VOLUME) {
		form_set_error('volume', t('Volume field must be less than or equal to ') . OP_VIDEO_MAX_VOLUME . '.');
	}
	
	if($form_values['bufferlength'] < 1) {
		form_set_error('bufferlength', t('Buffer length field must be greater than 0.'));
	}
	else if($form_values['bufferlength'] > OP_VIDEO_MAX_BUFFER_LENGTH) {
		form_set_error('bufferlength', t('Buffer length field must be less than or equal to ') . OP_VIDEO_MAX_BUFFER_LENGTH . '.');
	}
}


function _op_video_player_settings_submit(&$form_values) {

	settype($form_values['video_volume'], 'int');
	settype($form_values['video_bufferlength'], 'int');

	foreach(array('back', 'front', 'light') as $color) {
		$field = $pre . $color . 'color';
		$form_values["$field"] = strtoupper($form_values["$field"]);
	}
}


function _op_video_get_jwflash_vars($video, $autostart) {

	$flashvars[] = 'id=' . $video->video_id;
	$flashvars[] = 'callback=' . url('op_video/callback');
	$flashvars[] = 'file=' . file_create_url($video->flv_file->filepath);

	$width = _op_video_get_render_setting('player_width', OP_VIDEO_DEFAULT_PLAYER_WIDTH);
	$height = _op_video_get_render_setting('player_height', OP_VIDEO_DEFAULT_PLAYER_HEIGHT);
	$flashvars[] = 'displayheight=' . $height;

	$image = _op_video_get_preview_image($video);
	$image_filepath = _op_video_render_image($video, $image, $width, $height);

	if($image_filepath != '')
		$flashvars[] = 'image=' . file_create_url($image_filepath);
	
	if(_op_video_get_render_setting('controlbar', OP_VIDEO_DEFAULT_SHOW_CONTROLBAR))
		$height += 20;

	$options = array(
		'showicons' => OP_VIDEO_DEFAULT_SHOW_ICONS,
		'showdigits' => OP_VIDEO_DEFAULT_SHOW_DIGITS,
		'showfsbutton' => OP_VIDEO_DEFAULT_SHOW_FSBUTTON,
		'repeat' => OP_VIDEO_DEFAULT_AUTOREPEAT,
		'showvolume' => OP_VIDEO_DEFAULT_SHOWVOLUME
	);

	foreach($options as $option => $default) {
		$value = _op_video_get_render_setting($option, $default);
		$flashvars[] = $option . '=' . ($value ? 'true' : 'false');
	}

	if($autostart)
		$flashvars[] = 'autostart=' . _op_video_get_render_setting('autostart', OP_VIDEO_DEFAULT_AUTOSTART);

	$options = array(
		'volume' => OP_VIDEO_DEFAULT_VOLUME,
		'bufferlength' => OP_VIDEO_DEFAULT_BUFFER_LENGTH,
	);

	foreach($options as $option => $default) {
		$value = _op_video_get_render_setting($option, $default);
		$flashvars[] = $option . '=' . (int)$value;
	}

	$options = array(
		'backcolor' => OP_VIDEO_DEFAULT_BACKCOLOR,
		'frontcolor' => OP_VIDEO_DEFAULT_FRONTCOLOR,
		'lightcolor' => OP_VIDEO_DEFAULT_LIGHTCOLOR
	);

	foreach($options as $option => $default) {
		$value = _op_video_get_render_setting($option, $default);
		$flashvars[] = $option . '=0x' . $value;
	}
	
	
	if(_op_video_get_render_setting('showdownload', OP_VIDEO_DEFAULT_SHOWDOWNLOAD) && 
		!$video->source_file->deleted)
	{
		$flashvars[] = 'showdownload=true';
		$flashvars[] = 'link=' . url(_op_video_download_path($video));
	}

	$flashvars[] = 'overstretch=false';

	return array(
		'width' => $width,
		'height' => $height,
		'flashvars' => implode('&', $flashvars)
	);
}


function _op_video_get_player_url() {
	return base_path() . drupal_get_path('module', 'op_video') . '/jw_flv_player/flvplayer.swf';
}


function _op_video_render_preview_image_layer($video) {
	drupal_add_js(drupal_get_path('module', 'op_video') . '/ufo.js');
	drupal_add_js(drupal_get_path('module', 'op_video') . '/layer.js');

	$vars = _op_video_get_jwflash_vars($video, TRUE);
	$vars['flashvars'] = '&quot;' . $vars['flashvars'] . '&quot;';
	$vars[] = '&quot;' . _op_video_get_player_url() . '&quot;';
	$vars = implode(', ', $vars);

	$thumb = _op_video_render_preview_image($video, NULL, NULL, array('onclick="_op_video_play_layer(' . $vars . ');"', 'style="cursor: pointer;"'));
	return theme('op_video_preview_image', $thumb);
}


function _op_video_render_inline($video, $autostart) {
	drupal_add_js(drupal_get_path('module', 'op_video') . '/ufo.js');

	static $_jwflash_player_id;

	$player = "<div id=\"jwflashplayer$_jwflash_player_id\">";
	$player .= "Get <a href=\"http://www.macromedia.com/go/getflashplayer\">Flash</a> to see this video.";
	$player .= "</div>\n";
	$output = theme('op_video_player', $player);

	$player_url = _op_video_get_player_url();
	$vars = _op_video_get_jwflash_vars($video, $autostart);

	$output .= "<script type=\"text/javascript\">\n";
	$output .= "var FO = { movie:\"$player_url\",\n";
	$output .= "  width:\"" . $vars['width'] . "\",height:\"" . $vars['height'] . "\",majorversion:\"7\",build:\"0\",bgcolor:\"#FFFFFF\",\n";
	$output .= "  allowfullscreen:\"true\",flashvars:\"" . $vars['flashvars'] . "\" };\n";
	$output .= "UFO.create(FO,\"jwflashplayer$_jwflash_player_id\");\n";
	$output .= "</script>\n";

	$_jwflash_player_id++;
	
	return $output;
}

