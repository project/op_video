<?php


// Copyright 2008 Jonathan Brown


function _op_video_render_image($video, $image, $width, $height) {
	
	$file = $video->{$image . '_image_file'};

  $info = image_get_info($file->filepath);		// get rid of this
  if(!$info) {
    return;
  }
  
  if($width / $height > $info['width'] / $info['height']) {
  
  	if($height < $info['height']) {
			$scale_width = (int)round(($info['width'] * $height) / $info['height']);
			$scale_height = $height;
		}
		else {
			$scale_width = $info['width'];
			$scale_height = $info['height'];
			$width = (int)round(($width * $info['height']) / $height);
			$height = $scale_height;
		}

	 	$left = (int)round(($width - $scale_width) / 2);
	}
	else {
  	if($width < $info['width']) {
			$scale_height = (int)round(($info['height'] * $width) / $info['width']);
		  $scale_width = $width;
		}
		else {
			$scale_height = $info['height'];
			$scale_width = $info['width'];
			$height = (int)round(($height * $info['width']) / $width);
			$width = $scale_width;
		}

		$top = (int)round(($height - $scale_height) / 2);
	}
	
	$filepath = db_result(db_query("
		SELECT filepath
		FROM {op_video_image_cache}
		INNER JOIN {op_video_files}
			ON {op_video_image_cache}.file_id = {op_video_files}.file_id
		WHERE video_id = %d AND 
			image = '%s' AND
			width = %d AND
			height = %d
	",
		$video->video_id, $image, $width, $height
	));
	
	if($filepath)
		return $filepath;
	
	$filepath = _op_video_image_cache_path() . '/' . $video->video_id . '_' . $image . '_' . $width . 'x' . $height . '.jpeg';

	$src = image_gd_open($file->filepath, $info['extension']);
	$dest = imagecreatetruecolor($width, $height);
	imagecopyresampled($dest, $src, $left, $top, 0, 0, $scale_width, $scale_height, $info['width'], $info['height']);
  imagedestroy($src);
  imageinterlace($dest, TRUE);
  image_gd_close($dest, $filepath, 'jpeg');
  imagedestroy($dest);

	$file_id = _op_video_insert_filepath($filepath);
	
	db_query("
		INSERT INTO {op_video_image_cache}
		SET video_id = %d,
			image = '%s',
			width = %d,
			height = %d,
			file_id = %d
	",
		$video->video_id, $image, $width, $height, $file_id
	);
	
	return $filepath;
}


function _op_video_purge_image_cache($video_id = NULL) {

	if($video_id)
		$where = 'WHERE video_id = %d';

	$result = db_query("
		SELECT *
		FROM {op_video_image_cache}
		INNER JOIN {op_video_files}
			ON {op_video_image_cache}.file_id = {op_video_files}.file_id
		$where
	",
		$video_id
	);
	
	while($object = db_fetch_object($result)) {
		_op_video_delete_file_row($object);
	}
	
	db_query("
		DELETE FROM {op_video_image_cache}
		$where
	",
		$video_id
	);
}

