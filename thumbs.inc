<?php

// Copyright 2007 Jonathan Brown

define("OP_VIDEO_DEFAULT_THUMB_WIDTH", 320);
define("OP_VIDEO_DEFAULT_THUMB_HEIGHT", 240);
define("OP_VIDEO_DEFAULT_THUMB_IMAGE", 2);
define("OP_VIDEO_DEFAULT_THUMB_JPEG_QUALITY", '90');


function _op_video_thumb_settings_fields() {

	return array(
		'thumb_width',
		'thumb_height',
		'thumb_image',
		'thumb_jpeg_quality'
	);
}


function _op_video_thumb_settings_fieldset($settings = NULL, $pre = '') {

	$fieldset = array(
		'#type' => 'fieldset',
		'#title' => t('Thumbnail settings'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE
	);

  $fieldset[$pre . 'thumb_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => isset($settings['thumb_width']) ? $settings['thumb_width'] : OP_VIDEO_DEFAULT_THUMB_WIDTH,
//    '#description' => t('Show or hide the play and activity icons in the middle of the video.'),
		'#size' => 6,
		'#maxlength' => 4,
		'#required' => TRUE
  );

  $fieldset[$pre . 'thumb_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => isset($settings['thumb_height']) ? $settings['thumb_height'] : OP_VIDEO_DEFAULT_THUMB_HEIGHT,
//    '#description' => t('Show or hide the play and activity icons in the middle of the video.'),
		'#size' => 6,
		'#maxlength' => 4,
		'#required' => TRUE
  );
  
	$fieldset[$pre . 'thumb_image'] = array(
		'#type' => 'radios',
		'#title' => t('Image'),
		'#default_value' => isset($settings['thumb_image']) ? $settings['thumb_image'] : OP_VIDEO_DEFAULT_THUMB_IMAGE,
		'#options' => array(
			1 => 'First frame', 
			2 => 'Frame @ 1/3 of video duration'
		),
    '#required' => TRUE,
	);

	$fieldset[$pre . 'thumb_jpeg_quality'] = array(
		'#type' => 'textfield',
		'#title' => t('JPEG quality'),
		'#default_value' => isset($settings['thumb_jpeg_quality']) ? $settings['thumb_jpeg_quality'] : OP_VIDEO_DEFAULT_THUMB_JPEG_QUALITY,
		'#size' => 6,
		'#maxlength' => 3,
    '#field_suffix' => t('%'),
    '#required' => TRUE,
	);

  return $fieldset;
}


function _op_video_thumb_settings_validate($form_values, $pre = '') {
	;
}


function _op_video_thumb_invalidate($video_id = NULL) {

	if($video_id) {
	
		$result = db_query("
			SELECT splash_file_id, thumb_file_id
			FROM {op_videos}
			WHERE video_id = %d
		",
			$video_id
		);
	}
	else {
		$result = db_query("
			SELECT splash_file_id, thumb_file_id
			FROM {op_videos}
		");
	}
	
	while($object = db_fetch_object($result)) {
		
		db_query("
			UPDATE {op_video_files}
			SET dirty = 1
			WHERE file_id = %d
		",
			$object->splash_file_id
		);

		db_query("
			UPDATE {op_video_files}
			SET dirty = 1
			WHERE file_id = %d
		",
			$object->thumb_file_id
		);
	}
}


function _op_video_thumb_settings_submit(&$form_values, $pre = '') {

/*
	db_query("
		UPDATE {op_video_node_revisions}, {op_videos}, {op_video_files}
		SET {op_video_files}.dirty = 1
		WHERE {op_video_node_revisions}.video_id = {op_videos}.video_id AND
			{op_videos}.splash_file_id = {op_video_files}.file_id
	");

	db_query("
		UPDATE {op_video_node_revisions}, {op_videos}, {op_video_files}
		SET {op_video_files}.dirty = 1
		WHERE {op_video_node_revisions}.video_id = {op_videos}.video_id AND
			{op_videos}.thumb_file_id = {op_video_files}.file_id
	");
*/

	_op_video_thumb_invalidate();
}


function _op_video_render_thumb(&$video, $width, $height) {
	
	switch(_op_video_get_render_setting('thumb_image', OP_VIDEO_DEFAULT_THUMB_IMAGE)) {
	
		case 1:
			$file = $video->raw_image_1_file;
			break;
			
		case 2:
			$file = $video->raw_image_2_file;
			break;
	}	
	
  $info = image_get_info($file->filepath);
  
  if (!$info) {
    return FALSE;
  }
  
 	$new_height = (int)round(($info['height'] * $width) / $info['width']);
 	$top = (int)round(($height - $new_height) / 2);

  
	$dest = imagecreatetruecolor($width, $height);
	
	$src = @imagecreatefrompng($file->filepath);
	
	imagecopyresampled($dest, $src, 0, $top, 0, 0, $width, $new_height, $info['width'], $info['height']);
	
  imagedestroy($src);
  
	$filepath = _op_video_temp_path() . '/' . $video->video_id . '_thumb.jpeg';
  imageinterlace($dest, TRUE);
  imagejpeg($dest, $filepath, _op_video_get_render_setting('thumb_jpeg_quality', OP_VIDEO_DEFAULT_THUMB_JPEG_QUALITY));
  imagedestroy($dest);
  file_move($filepath, _op_video_transcoded_path(), FILE_EXISTS_REPLACE);
	$file_id = _op_video_insert_filepath($filepath);

	db_query("
		UPDATE {op_videos}
		SET thumb_file_id = %d
		WHERE video_id = %d
	",
		$file_id,
		$video->video_id
	);

	$video->thumb_file_id = $file_id;
	$video->thumb_file = _op_video_get_file_info($file_id);
}


function _op_video_render_splash(&$video, $width, $height) {

	switch(_op_video_get_render_setting('thumb_image', OP_VIDEO_DEFAULT_THUMB_IMAGE)) {
	
		case 1:
			$file = $video->raw_image_1_file;
			break;
			
		case 2:
			$file = $video->raw_image_2_file;
			break;
	}	
	
  $info = image_get_info($file->filepath);
  if (!$info) {
    return FALSE;
  }
  
//  var_dump($info, $width, $height);
  
  if($width > $info['width']) {
  	$height = (int)round(($height * $info['width']) / $width);
  	$width = $info['width'];
  }
  
 	$new_height = (int)round(($info['height'] * $width) / $info['width']);
 	$top = (int)round(($height - $new_height) / 2);

/*
 	$new_width = (int)round(($info['width'] * $height) / $info['height']);
 	$left = (int)round(($width - $new_width) / 2);
*/

	$dest = imagecreatetruecolor($width, $height);
	
	$src = @imagecreatefrompng($file->filepath);
	
	imagecopyresampled($dest, $src, 0, $top, 0, 0, $width, $new_height, $info['width'], $info['height']);
	
  imagedestroy($src);
  
	$filepath = _op_video_temp_path() . '/' . $video->video_id . '_splash.jpeg';
  imagejpeg($dest, $filepath, _op_video_get_render_setting('thumb_jpeg_quality', OP_VIDEO_DEFAULT_THUMB_JPEG_QUALITY));
  imagedestroy($dest);
  file_move($filepath, _op_video_transcoded_path(), FILE_EXISTS_REPLACE);
	$file_id = _op_video_insert_filepath($filepath);

	db_query("
		UPDATE {op_videos}
		SET splash_file_id = %d
		WHERE video_id = %d
	",
		$file_id,
		$video->video_id
	);

	$video->splash_file_id = $file_id;
	$video->splash_file = _op_video_get_file_info($file_id);
}


function _op_video_render_thumbnail($video, $extra = NULL) {

	$width = _op_video_get_render_setting('thumb_width', OP_VIDEO_DEFAULT_THUMB_WIDTH);
	$height = _op_video_get_render_setting('thumb_height', OP_VIDEO_DEFAULT_THUMB_HEIGHT);
	
	if(_op_video_is_file_dirty($video->thumb_file)) {
		_op_video_render_thumb($video, $width, $height);
	}

	$parts[] = '<img';
	$parts[] = 'src="' . file_create_url($video->thumb_file->filepath) . '"';
	$parts[] = 'width="' . $width . '"';
	$parts[] = 'height="' . $height . '"';
	
	if(is_array($extra))
		$parts = array_merge($parts, $extra);

	$parts[] = '/>';

	return implode(' ', $parts); 
}

