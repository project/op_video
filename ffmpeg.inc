<?php

// Copyright 2007 Jonathan Brown

define("OP_VIDEO_DEFAULT_FFMPEG_BINARY", 'ffmpeg');


function _op_video_ffmpeg_settings_fieldset() {

	$fieldset = array(
		'#type' => 'fieldset',
		'#title' => t('Local FFmpeg installation'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE
	);

	$fieldset['op_video_ffmpeg_binary'] = array(
		'#type' => 'textfield',
		'#title' => t('FFmpeg binary'),
		'#description' => t('If the binary is not in the default path, you should enter the absolute path to it.'),
		'#default_value' => variable_get('op_video_ffmpeg_binary', OP_VIDEO_DEFAULT_FFMPEG_BINARY),
	);

	$fieldset['op_video_ffmpeg_extra_params'] = array(
		'#type' => 'textfield',
		'#title' => t('Extra parameters (advanced)'),
		'#description' => t('Any extra <a href="http://ffmpeg.mplayerhq.hu/ffmpeg-doc.html">parameters</a> you wish to have appended onto the FFmpeg command.'),
		'#default_value' => variable_get('op_video_ffmpeg_extra_params', ''),
	);
	
	return $fieldset;
}


function _op_video_ffmpeg_exec($params, $mode, &$output, &$result) {

	$ffmpeg = variable_get('op_video_ffmpeg_binary', OP_VIDEO_DEFAULT_FFMPEG_BINARY);
	$extra_params = variable_get('op_video_ffmpeg_extra_params', '');

	$command = array($ffmpeg, $params, $extra_params);

	_op_video_exec($command, $mode, $output, $result);
}


function _op_video_ffmpeg_get_info($source_file) {
	
	$params = '-i ' . escapeshellarg($source_file->filepath);
	_op_video_ffmpeg_exec($params, OP_VIDEO_EXEC_MODE_NO_ECHO, $output, $result);
	array_shift($output);
	$output = implode("\n", $output);
	
	if(preg_match('/Duration: *(..):(..):(..\..), /', $output, $matches))
		$info['duration'] = ($matches[1] * 60 + $matches[2]) * 60 + $matches[3];
	else
		return NULL;

	if(preg_match('/Audio: (.*)/', $output, $matches)) {
		$parts = explode(', ', $matches[1]);
		
		$info['audio_codec'] = strtok(array_shift($parts), ' ');
		$info['audio_sample_rate'] = strtok(array_shift($parts), ' ');
		$info['audio_channels'] = array_shift($parts);
		
		switch($info['audio_channels']) {
			
			case 'mono':
				$info['audio_channels'] = 1;
				break;

			case 'stereo':
				$info['audio_channels'] = 2;
				break;
		}
		
		$info['audio_bitrate'] = strtok(array_shift($parts), ' ');
	}
	else {
		$info['audio_channels'] = 0;
	}

	if(preg_match('/Video: (.*)/', $output, $matches)) {
		$parts = explode(', ', $matches[1]);
		
		$info['video_codec'] = array_shift($parts);
		array_shift($parts);
		$dims = explode('x', array_shift($parts));
		$info['width'] = $dims[0];
		$info['height'] = strtok($dims[1], ' ');
		
		if(count($parts) > 1)
			$info['video_bitrate'] = strtok(array_shift($parts), ' ');
			
		$info['frame_rate'] = strtok(array_shift($parts), ' ');
	}
	
	$info['video_bitrate'] = $source_file->filesize * 8 / 1000 / $info['duration'] - $info['audio_bitrate'];

	$info['source_file'] = $source_file;
	
	return $info;
}


function _op_video_ffmpeg_create_png($id, $info, $i, &$output) {
	
	$params[] = '-y -i ' . escapeshellarg($info['source_file']->filepath);

	$out_file = _op_video_temp_path() . '/' . $id . '_raw_image_' . $i . '.png';

	if($i == 2)
		$params[] = '-ss ' . $info['duration'] / 3;

	$params[] = '-vcodec png';
	$params[] = '-vframes 1';
	$params[] = '-f rawvideo';
	$params[] = '-an';
	$params[] = $out_file;

	_op_video_ffmpeg_exec($params, OP_VIDEO_EXEC_MODE_ECHO_AND_STORE, $output, $result);

	if($result == 0) {
		file_move($out_file, _op_video_transcoded_path(), FILE_EXISTS_REPLACE);
		return _op_video_insert_filepath($out_file);
	}
	else {
		file_delete($out_file);
		return FALSE;
	}
}


function _op_video_ffmpeg_create_flv($id, $info, $dirty_params, &$output) {

	$params[] = '-y -i ' . escapeshellarg($info['source_file']->filepath);

	$out_file = _op_video_temp_path() . '/' . $id . '.flv';

	$channels = min($info['audio_channels'], $dirty_params['audio_channels']);

	if($dirty_params['max_duration'] != 0 && 
		$info['duration'] > $dirty_params['max_duration'])
	{
		$params[] = '-t ' . $dirty_params['max_duration'];
	}

	if($channels == 0) {
		$params[] = '-an';
	}
	else if($info['audio_codec'] == 'mp3' &&
		$info['audio_bitrate'] <= $dirty_params['audio_bitrate'] &&
		$info['audio_channels'] <= $dirty_params['audio_channels'] &&
		$info['audio_sample_rate'] <= $dirty_params['audio_sample_rate'] &&
		in_array($info['audio_sample_rate'], array(11025, 22050, 44100)))
	{
		$params[] = '-acodec copy';
	}
	else {
		$params[] = '-ab ' . $dirty_params['audio_bitrate'] . 'k';
		$params[] = '-ac ' . $channels;
		$params[] = '-ar ' . $dirty_params['audio_sample_rate'];
	}
	
	if($info['video_codec'] == 'flv' &&
		$info['video_bitrate'] <= $dirty_params['video_bitrate'] &&
		$info['width'] <= $dirty_params['width'] &&
		$info['frame_rate'] <= $dirty_params['frame_rate'])
	{
		$params[] = '-vcodec copy';
		$params[] = $out_file;
		_op_video_ffmpeg_exec($params, OP_VIDEO_EXEC_MODE_ECHO_AND_STORE, $output, $result);
	}
	else {
		$params[] = '-b ' . $dirty_params['video_bitrate'] . 'k';

		if($info['frame_rate'] > $dirty_params['frame_rate']) {
			$frame_rate = $dirty_params['frame_rate'];
			$params[] = '-r ' . $frame_rate;
		}
		else {
			$frame_rate = $info['frame_rate'];
		}
		
		$params[] = '-g ' . (int)round($frame_rate * $dirty_params['key_interval']);

		if($info['width'] > $dirty_params['width']) {

			$width = ((int)($dirty_params['width'] / 2)) * 2;
			$height = (int)round(($dirty_params['width'] * $info['height']) / $info['width']);
			$height = ((int)($height / 2)) * 2;
			
			$params[] = '-s ' . $width . 'x' . $height;
		}

		$params[] = $out_file;

		$log_file = _op_video_temp_path() . '/2pass.' . $id . '.log';

		_op_video_ffmpeg_exec(array($params, '-pass 1 -passlogfile ' . $log_file), 
			OP_VIDEO_EXEC_MODE_ECHO_AND_STORE, $output, $result);

		if($result == 0) {
			_op_video_ffmpeg_exec(array($params, '-pass 2 -passlogfile ' . $log_file), 
				OP_VIDEO_EXEC_MODE_ECHO_AND_STORE, $output, $result);

			if($result != 0) {	// dual pass failed - try single pass
				_op_video_ffmpeg_exec($params, OP_VIDEO_EXEC_MODE_ECHO_AND_STORE, $output, $result);
			}
		}

		file_delete($log_file);
	}
	
	if($result == 0) {
		file_move($out_file, _op_video_transcoded_path(), FILE_EXISTS_REPLACE);
		return _op_video_insert_filepath($out_file);
	}
	else {
		file_delete($out_file);
		return FALSE;
	}
}


function _op_video_transcode_ffmpeg($video) {

	$info = _op_video_ffmpeg_get_info($video->source_file);
	
	if($info) {
		$flv_file_id = _op_video_ffmpeg_create_flv($video->video_id, $info, $video->dirty_params, $output);

		if($flv_file_id) {
			$start_image_file_id = _op_video_ffmpeg_create_png($video->video_id, $info, 1, $output);
			$third_image_file_id = _op_video_ffmpeg_create_png($video->video_id, $info, 2, $output);

			$current_params_id = _op_video_transcoding_params_insert($video->dirty_params);

			db_query("
				UPDATE {op_videos}
				SET duration = %d,
					flv_file_id = %d,
					start_image_file_id = %d,
					third_image_file_id = %d,
					current_params_id = %d,
					status = 'idle',
					error = 'none'
				WHERE video_id = %d
			",
				(int)ceil($info['duration']),
				$flv_file_id,
				$start_image_file_id,
				$third_image_file_id,
				$current_params_id,
				$video->video_id
			);

			_op_video_transcoding_completed($video);
			return;
		}
	}

	db_query("
		UPDATE {op_videos}
		SET status = 'idle',
			error = 'transcoding failed'
		WHERE video_id = %d
	",
		$video->video_id
	);
}

