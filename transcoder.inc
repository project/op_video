<?php

// Copyright 2007 Jonathan Brown

define("OP_VIDEO_DEFAULT_WIDTH", 320);
define("OP_VIDEO_DEFAULT_FRAME_RATE", 24);
define("OP_VIDEO_DEFAULT_KEYINT", 2);
define("OP_VIDEO_DEFAULT_VBITRATE", 320);
define("OP_VIDEO_DEFAULT_CHANNELS", 2);
define("OP_VIDEO_DEFAULT_SAMPLE_RATE", 44100);
define("OP_VIDEO_DEFAULT_ABITRATE", 64);
define("OP_VIDEO_DEFAULT_MAX_DURATION", 0);
define("OP_VIDEO_DEFAULT_KEEP_ORIGINAL", 1);

define("OP_VIDEO_MIN_DIMENSION", 32);
define("OP_VIDEO_MAX_DIMENSION", 4096);
define("OP_VIDEO_MAX_FRAME_RATE", 200);
define("OP_VIDEO_MAX_KEYINT", 600);
define("OP_VIDEO_MIN_VBITRATE", 32);
define("OP_VIDEO_MAX_VBITRATE", 4096);
define("OP_VIDEO_MIN_ABITRATE", 16);
define("OP_VIDEO_MAX_ABITRATE", 512);

_op_video_require('ffmpeg.inc');
_op_video_require('openpackage.inc');


function _op_video_transcoding_params() {

	return array(
		'width',
		'frame_rate',
		'key_interval',
		'video_bitrate',
		'audio_channels',
		'audio_sample_rate',
		'audio_bitrate',
		'max_duration',
		'keep_source_file',
	);
}


function _op_video_transcoding_params_formats() {

	return array(
		'width' => "%d",
		'frame_rate' => "%d",
		'key_interval' => "'%s'",				// decimal
		'video_bitrate' => "%d",
		'audio_channels' => "'%s'",			// enum
		'audio_sample_rate' => "'%s'",	// enum
		'audio_bitrate' => "%d",
		'max_duration' => "%d",
		'keep_source_file' => "%d"
	);
}


function _op_video_transcoding_params_defaults() {

	return array(
		'width' => OP_VIDEO_DEFAULT_WIDTH,
		'frame_rate' => OP_VIDEO_DEFAULT_FRAME_RATE,
		'key_interval' => OP_VIDEO_DEFAULT_KEYINT,
		'video_bitrate' => OP_VIDEO_DEFAULT_VBITRATE,
		'audio_channels' => OP_VIDEO_DEFAULT_CHANNELS,
		'audio_sample_rate' => OP_VIDEO_DEFAULT_SAMPLE_RATE,
		'audio_bitrate' => OP_VIDEO_DEFAULT_ABITRATE,
		'max_duration' => OP_VIDEO_DEFAULT_MAX_DURATION,
		'keep_source_file' => OP_VIDEO_DEFAULT_KEEP_ORIGINAL
	);
}


function _op_video_transcoding_params_load($params_id) {
	
	return (array)db_fetch_object(db_query("
		SELECT *
		FROM {op_video_params}
		WHERE params_id = %d
	",
		$params_id
	));
}


function _op_video_transcoding_params_insert($params = NULL) {
	if(!$params)
		$params = _op_video_transcoding_params_defaults();
	
	$fields = _op_video_transcoding_params_formats();
	return _op_video_db_insert('op_video_params', $fields, $params);
}


function _op_video_transcoding_params_update($params_id, &$params) {

	$fields = _op_video_transcoding_params_formats();
	_op_video_db_update('op_video_params', 'params_id', $params_id, $fields, $params, TRUE);
}


function _op_video_transcoding_params_update_defaults($params_id) {
	
	$fields = _op_video_transcoding_params_formats();
	$params = _op_video_transcoding_params_defaults();
	_op_video_db_update('op_video_params', 'params_id', $params_id, $fields, $params, TRUE);
}


function _op_video_transcoding_params_delete($params_id) {
	
	db_query("
		DELETE FROM {op_video_params}
		WHERE params_id = %d
	",
		$params_id
	);
}


function _op_video_transcoding_params_fieldset($params = NULL) {

	$fieldset = array(
		'#type' => 'fieldset',
		'#title' => t('Transcoding parameters'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE
	);

	$fieldset['width'] = array(
		'#type' => 'textfield',
		'#title' => t('Video width'),
		'#default_value' => isset($params['width']) ? $params['width'] : OP_VIDEO_DEFAULT_WIDTH,
    '#required' => TRUE,
		'#size' => 6,
		'#maxlength' => 4,
		'#attributes' => array('autocomplete' => 'off'),
		'#description' => t("(pixels)<br />Must be multiple of 2.<br />The height is determined automatically by the aspect ratio of each video.")
	);

	$fieldset['frame_rate'] = array(
		'#type' => 'textfield',
		'#title' => t('Maximum frame rate'),
		'#default_value' => isset($params['frame_rate']) ? $params['frame_rate'] : OP_VIDEO_DEFAULT_FRAME_RATE,
    '#required' => TRUE,
		'#size' => 6,
		'#maxlength' => 3,
		'#attributes' => array('autocomplete' => 'off'),
		'#description' => t("Maximum frames per second. If the rate of the source video is less, that rate is used.")
	);

	$fieldset['key_interval'] = array(
		'#type' => 'textfield',
		'#title' => t('Key frame interval'),
		'#default_value' => isset($params['key_interval']) ? $params['key_interval'] : OP_VIDEO_DEFAULT_KEYINT,
    '#required' => TRUE,
		'#size' => 6,
		'#maxlength' => 5,
		'#attributes' => array('autocomplete' => 'off'),
		'#description' => t("Maximum interval between seekable frames (seconds).")
	);

	$fieldset['video_bitrate'] = array(
		'#type' => 'textfield',
		'#title' => t('Video bitrate'),
		'#default_value' => isset($params['video_bitrate']) ? $params['video_bitrate'] : OP_VIDEO_DEFAULT_VBITRATE,
    '#required' => TRUE,
		'#size' => 6,
		'#maxlength' => 4,
		'#attributes' => array('autocomplete' => 'off'),
		'#description' => t("kbits, 1 kbit = 1000 bits")
	);

	$fieldset['audio_channels'] = array(
		'#type' => 'radios',
		'#title' => t('Audio channels'),
		'#default_value' => isset($params['audio_channels']) ? $params['audio_channels'] : OP_VIDEO_DEFAULT_CHANNELS,
		'#options' => array(
			'0' => 'Mute', 
			'1' => 'Mono', 
			'2' => 'Stereo'
		),
    '#required' => TRUE,
	);

	$fieldset['audio_sample_rate'] = array(
		'#type' => 'radios',
		'#title' => t('Audio sample rate'),
		'#default_value' => isset($params['audio_sample_rate']) ? $params['audio_sample_rate'] : OP_VIDEO_DEFAULT_SAMPLE_RATE,
		'#options' => array(
			'11025' => '11,025 Hz', 
			'22050' => '22,050 Hz', 
			'44100' => '44,100 Hz'
		),
    '#required' => TRUE,
	);

	$fieldset['audio_bitrate'] = array(
		'#type' => 'textfield',
		'#title' => t('Audio bitrate'),
		'#default_value' => isset($params['audio_bitrate']) ? $params['audio_bitrate'] : OP_VIDEO_DEFAULT_ABITRATE,
    '#required' => TRUE,
		'#size' => 6,
		'#maxlength' => 3,
		'#attributes' => array('autocomplete' => 'off'),
		'#description' => t("kbits, 1 kbit = 1000 bits")
	);
	
	$value = isset($params['max_duration']) ? $params['max_duration'] : OP_VIDEO_DEFAULT_MAX_DURATION;
	
	$fieldset['max_duration'] = array(
		'#type' => 'textfield',
		'#title' => t('Max duration'),
		'#default_value' => $value != 0 ? $value : '',
		'#size' => 6,
		'#maxlength' => 5,
		'#attributes' => array('autocomplete' => 'off'),
		'#description' => t("Maximum duration of transcoded video (seconds).")
	);
		
	$description = 'Should uploaded video files be kept once they have been transcoded to Flash?<br />';
	$description .= 'This is necessary for videos to be available for download and ';
	$description .= 'also for retranscoding if transcoding settings are changed.';

	$fieldset['keep_source_file'] = array(
		'#type' => 'checkbox',
		'#title' => t('Keep uploaded files?'),
		'#default_value' => isset($params['keep_source_file']) ? $params['keep_source_file'] : OP_VIDEO_DEFAULT_KEEP_ORIGINAL,
		'#description' => t($description)
	);

	$fieldset['break'] = array(
		'#value' => '<hr />'
	);

	$fieldset['retranscode_all'] = array(
		'#type' => 'checkbox',
		'#title' => t('Retranscode all videos?'),
		'#description' => t("This will retranscode all the videos that these parameters apply to that have their source video stored.")
	);

	return $fieldset;
}


function _op_video_transcoding_params_validate($form_values) {

	settype($form_values['width'], 'int');
	settype($form_values['frame_rate'], 'int');
	$form_values['key_interval'] = number_format($form_values['key_interval'], 1, '.', '');
	settype($form_values['video_bitrate'], 'int');
	settype($form_values['audio_bitrate'], 'int');

	if($form_values['width'] < OP_VIDEO_MIN_DIMENSION) {
		form_set_error('width', t('Video width field must be greater than or equal to ') . OP_VIDEO_MIN_DIMENSION . '.');
	}
	else if($form_values['width'] > OP_VIDEO_MAX_DIMENSION) {
		form_set_error('width', t('Video width field must be less than or equal to ') . OP_VIDEO_MAX_DIMENSION . '.');
	}
	else if($form_values['width'] % 2 != 0) {
		form_set_error('width', t('Video width field must be a multiple of 2.'));
	}

	if($form_values['frame_rate'] <= 0) {
		form_set_error('frame_rate', t('Video frame rate field must be greater than 0.'));
	}
	else if($form_values['frame_rate'] > OP_VIDEO_MAX_FRAME_RATE) {
		form_set_error('frame_rate', t('Video frame rate field must be less than or equal to ') . OP_VIDEO_MAX_FRAME_RATE . '.');
	}

	if($form_values['key_interval'] <= 0) {
		form_set_error('key_interval', t('Key frame interval field must be greater than 0.'));
	}
	else if($form_values['key_interval'] > OP_VIDEO_MAX_KEYINT) {
		form_set_error('key_interval', t('Key frame interval field must be less than or equal to ') . OP_VIDEO_MAX_KEYINT . '.');
	}

	if($form_values['video_bitrate'] < OP_VIDEO_MIN_VBITRATE) {
		form_set_error('video_bitrate', t('Video bitrate field must be greater than or equal to ') . OP_VIDEO_MIN_VBITRATE . '.');
	}
	else if($form_values['video_bitrate'] > OP_VIDEO_MAX_VBITRATE) {
		form_set_error('video_bitrate', t('Video bitrate field must be less than or equal to ') . OP_VIDEO_MAX_VBITRATE . '.');
	}

	if($form_values['audio_bitrate'] < OP_VIDEO_MIN_ABITRATE) {
		form_set_error('audio_bitrate', t('Audio bitrate field must be greater than or equal to ') . OP_VIDEO_MIN_ABITRATE . '.');
	}
	else if($form_values['audio_bitrate'] > OP_VIDEO_MAX_ABITRATE) {
		form_set_error('audio_bitrate', t('Audio bitrate field must be less than or equal to ') . OP_VIDEO_MAX_ABITRATE . '.');
	}
}


function _op_video_transcode_videos() {

	$transcoder = variable_get('op_video_transcoder', OP_VIDEO_DEFAULT_TRANSCODER);

	$videos_result = db_query("
  	SELECT *
  	FROM {op_videos}
  	WHERE status = 'dirty'
		ORDER BY video_id
		LIMIT 1
	");

	while($video = db_fetch_object($videos_result)) {

		$video->source_file = _op_video_get_file_info($video->source_file_id);
		$video->dirty_params = _op_video_transcoding_params_load($video->dirty_params_id);

		switch($transcoder) {

			case 'ffmpeg':
				_op_video_transcode_ffmpeg($video);
				break;

			case 'openpackage.biz':
				_op_video_transcode_openpackage($video);
				break;
		}
	}
}


function _op_video_transcoding_completed($video) {

	$video = _op_video_load($video);
	
	if(!$video->dirty_params['keep_source_file'])
		_op_video_delete_file($video->source_file);
	
	
	_op_video_purge_image_cache($video->video_id);
}

