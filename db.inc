<?php

// Copyright 2007 Jonathan Brown

function _op_video_db_insert_id() {
	global $active_db;

	// You bounder!
	// You cheat!

	switch ($GLOBALS['db_type']) {
		case 'mysql':
			return mysql_insert_id($active_db);
		case 'mysqli':
			return mysqli_insert_id($active_db);
	}
	
	// TODO: check for error
}


function _op_video_db_insert($table, $fields = NULL, $data = NULL) {

	if(is_array($fields)) {
		foreach($fields as $field => $format) {
			if(isset($data[$field])) {
				$sql_params[] = $field . ' = ' . $format;
				$sql_values[] = $data[$field];
			}
		}
	}

	if(count($sql_params)) {
		$sql_params = 'SET ' . implode(',', $sql_params);

		db_query("
			INSERT INTO {{$table}}
			$sql_params
		",
			$sql_values
		);
	}
	else {
		db_query("
			INSERT INTO {{$table}}
			() VALUES ()
		");
	}

	return _op_video_db_insert_id();
}


function _op_video_db_update($table, $id_field, $id, $fields, &$data, $unset = FALSE) {

	foreach($fields as $field => $format) {
		if(isset($data[$field])) {
			$sql_params[] = $field . ' = ' . $format;
			$sql_values[] = $data[$field];
			
			if($unset)
				unset($data[$field]);
		}
	}

	$sql_params = implode(',', $sql_params);
	$sql_values[] = $id;

	db_query("
		UPDATE {{$table}}
		SET $sql_params
		WHERE $id_field = %d
	",
		$sql_values
	);
}

