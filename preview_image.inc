<?php

// Copyright 2007 Jonathan Brown

define("OP_VIDEO_DEFAULT_PREVIEW_IMAGE_WIDTH", 320);
define("OP_VIDEO_DEFAULT_PREVIEW_IMAGE_HEIGHT", 240);
define("OP_VIDEO_DEFAULT_PREVIEW_IMAGE_IMAGE", 'custom');
define("OP_VIDEO_DEFAULT_PREVIEW_IMAGE_USER_SELECTABLE", 1);

function _op_video_preview_image_settings_fields() {

	return array(
		'preview_image_width',
		'preview_image_height',
		'preview_image_image',
		'preview_image_user_selectable'
	);
}


function _op_video_preview_image_settings_fieldset($settings = NULL) {

	$fieldset = array(
		'#type' => 'fieldset',
		'#title' => t('Preview image'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE
	);

  $fieldset['preview_image_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => isset($settings['preview_image_width']) ? $settings['preview_image_width'] : OP_VIDEO_DEFAULT_PREVIEW_IMAGE_WIDTH,
//    '#description' => t('Show or hide the play and activity icons in the middle of the video.'),
		'#size' => 6,
		'#maxlength' => 4,
		'#required' => TRUE
  );

  $fieldset['preview_image_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => isset($settings['preview_image_height']) ? $settings['preview_image_height'] : OP_VIDEO_DEFAULT_PREVIEW_IMAGE_HEIGHT,
//    '#description' => t('Show or hide the play and activity icons in the middle of the video.'),
		'#size' => 6,
		'#maxlength' => 4,
		'#required' => TRUE
  );
  
	$fieldset['preview_image_image'] = array(
		'#type' => 'radios',
		'#title' => t('Image'),
		'#default_value' => isset($settings['preview_image_image']) ? $settings['preview_image_image'] : OP_VIDEO_DEFAULT_PREVIEW_IMAGE_IMAGE,
		'#options' => array(
			'start' => 'Start of video',
			'third' => '@ 1/3 of video duration'
		)
	);

	$fieldset['preview_image_user_selectable'] = array(
		'#type' => 'checkbox',
		'#title' => 'User selectable',
		'#default_value' => isset($settings['preview_image_user_selectable']) ? $settings['preview_image_user_selectable'] : OP_VIDEO_DEFAULT_PREVIEW_IMAGE_USER_SELECTABLE,
	);
    
  return $fieldset;
}


function _op_video_preview_image_settings_validate($form_values) {
	;
}


function _op_video_preview_image_settings_submit(&$form_values) {
	_op_video_purge_image_cache();
}


function _op_video_set_preview_image($video_id, $preview_image) {
	
	db_query("
		UPDATE {op_videos}
		SET preview_image = '%s'
		WHERE video_id = %d
	",
		$preview_image,
		$video_id
	);
}


function _op_video_get_preview_image($video) {
	if(_op_video_get_render_setting('preview_image_user_selectable', OP_VIDEO_DEFAULT_PREVIEW_IMAGE_USER_SELECTABLE)) {
		return $video->preview_image;
	}
	else {
		return _op_video_get_render_setting('preview_image_image', OP_VIDEO_DEFAULT_PREVIEW_IMAGE_IMAGE);
	}
}


function _op_video_render_preview_image($video, $width = NULL, $height = NULL, $extra = NULL, $image = NULL) {

	if(!$width)
		$width = _op_video_get_render_setting('preview_image_width', OP_VIDEO_DEFAULT_PREVIEW_IMAGE_WIDTH);
		
	if(!$height)
		$height = _op_video_get_render_setting('preview_image_height', OP_VIDEO_DEFAULT_PREVIEW_IMAGE_HEIGHT);
	
	if(!$image)
		$image = _op_video_get_preview_image($video);
	
	$filepath = _op_video_render_image($video, $image, $width, $height);

	$parts[] = '<img';
	$parts[] = 'src="' . file_create_url($filepath) . '"';
	$parts[] = 'width="' . $width . '"';
	$parts[] = 'height="' . $height . '"';
	
	if(is_array($extra))
		$parts = array_merge($parts, $extra);

	$parts[] = '/>';

	return implode(' ', $parts); 
}


function _op_video_render_thumbnail($video, $width = NULL, $height = NULL, $extra = NULL) {
	return _op_video_render_preview_image($video, $width, $height, $extra);
}


