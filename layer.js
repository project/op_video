
// Copyright 2007 Jonathan Brown


function f_clientWidth() {
	return f_filterResults (
		window.innerWidth ? window.innerWidth : 0,
		document.documentElement ? document.documentElement.clientWidth : 0,
		document.body ? document.body.clientWidth : 0
	);
}
function f_clientHeight() {
	return f_filterResults (
		window.innerHeight ? window.innerHeight : 0,
		document.documentElement ? document.documentElement.clientHeight : 0,
		document.body ? document.body.clientHeight : 0
	);
}
function f_scrollLeft() {
	return f_filterResults (
		window.pageXOffset ? window.pageXOffset : 0,
		document.documentElement ? document.documentElement.scrollLeft : 0,
		document.body ? document.body.scrollLeft : 0
	);
}
function f_scrollTop() {
	return f_filterResults (
		window.pageYOffset ? window.pageYOffset : 0,
		document.documentElement ? document.documentElement.scrollTop : 0,
		document.body ? document.body.scrollTop : 0
	);
}
function f_filterResults(n_win, n_docel, n_body) {
	var n_result = n_win ? n_win : 0;
	if (n_docel && (!n_result || (n_result > n_docel)))
		n_result = n_docel;
	return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
}


function _op_video_play_layer(width, height, flashvars, player_url) {

	function kill_layer() {
		document.body.removeChild(layer);
	}

	var layer = document.createElement('div');
  layer.style.position = "absolute";
	layer.style.left = "0px";
	layer.style.top = "0px";
	layer.style.width = document.body.clientWidth + "px";
	layer.style.height = document.body.clientHeight + "px";
	layer.style.background = "#000";
	layer.style.zIndex = "1000";
	layer.style.opacity = "0.75";
	layer.style.filter = "alpha(opacity=75)";
	layer.style.cursor = "pointer";
	layer.onclick = kill_layer;
  document.body.appendChild(layer);
  
	var player = document.createElement('div');
	player.id = "inline-player";
  player.style.position = "absolute";
	player.style.left = f_scrollLeft() + parseInt((f_clientWidth() - width) / 2) + "px";
	player.style.top = f_scrollTop() + parseInt((f_clientHeight() - height) / 2) + "px";
	player.style.opacity = "1.00";
	player.style.filter = "alpha(opacity=100)";
  layer.appendChild(player);
 
 	var FO = {
		movie: player_url,
  	width: width,
  	height: height,
  	majorversion: "7",
  	build: "0",
  	bgcolor: "#3F3F3F",
  	allowfullscreen: "true",
  	quality: "high",
  	flashvars: flashvars
 	};
	
	UFO.create(FO, "inline-player");
}

