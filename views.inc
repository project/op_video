<?php

function _op_video_views_op_videos_fields($name) {

	$fields = array(
		'inline_player' => array(
		  'field' => 'video_id',
		  'name' => $name . t(': Inline player'),
		  'help' => t('-'),
		  'handler' => '_op_video_views_handler_inline_player'
		),

		'thumbnail_layer' => array(
		  'field' => 'video_id',
		  'name' => $name . t(': Thumbnail - player in layer'),
		  'help' => t('-'),
		  'handler' => '_op_video_views_handler_thumbnail_layer',
		  'option' => _op_video_dimensions_form()
		),

		'thumbnail_link' => array(
		  'field' => 'video_id',
		  'name' => $name . t(': Thumbnail - link to node'),
		  'help' => t('-'),
		  'handler' => '_op_video_views_handler_thumbnail_link',
		  'option' => _op_video_dimensions_form()
		),

		'duration' => array(
		  'name' => $name . t(': Duration'),
		  'help' => t('-'),
		  'sortable' => TRUE,
		  'handler' => '_op_video_views_handler_duration',
		  'option' => 'integer',
		),
		'views' => array(
		  'name' => $name . t(': Total views'),
		  'help' => t('-'),
		  'sortable' => TRUE,
		),
		'views_completed' => array(
		  'name' => $name . t(': Total views completed'),
		  'help' => t('-'),
		  'sortable' => TRUE,
		),
		'downloads' => array(
		  'name' => $name . t(': Total downloads'),
		  'help' => t('-'),
		  'sortable' => TRUE,
		),

		'views_duration' => array(
		  'name' => $name . t(': Views in duration'),
		  'help' => t('-'),
		  'sortable' => FALSE,
		  'field' => 'video_id',
		  'handler' => '_op_video_views_handler_stats',
		  'option' => _op_video_duration_form()
		),

		'views_completed_duration' => array(
		  'name' => $name . t(': Complete views in duration'),
		  'help' => t('-'),
		  'sortable' => FALSE,
		  'field' => 'video_id',
		  'handler' => '_op_video_views_handler_stats',
		  'option' => _op_video_duration_form()
		),

		'downloads_duration' => array(
		  'name' => $name . t(': Downloads in duration'),
		  'help' => t('-'),
		  'sortable' => FALSE,
		  'field' => 'video_id',
		  'handler' => '_op_video_views_handler_stats',
		  'option' => _op_video_duration_form()
		)
	);
	
	return $fields;
}


function _op_video_views_op_video_files_table($field, $name) {

	$table = array(
		'name' => 'op_video_files',
		'join' => array(
			'left' => array(
				'table' => $field . '_op_videos',
				'field' => 'source_file_id'
			),
			'right' => array(
				'field' => 'file_id'
			)
		),
    'fields' => array(
      'filesize' => array(
        'name' => $name . t(': Filesize'),
        'help' => t('-'),
        'sortable' => TRUE,
        'handler' => '_op_video_views_handler_bytesize'
			),
    )
  );
  
  return $table;
}


function _op_video_views_handler_inline_player($fieldinfo, $fielddata, $value, $data) {

//	var_dump($fieldinfo, $fielddata, $value, $data);

	$video = _op_video_load($value);

	return _op_video_render_video($video, 'default', $data->nid);
}


function _op_video_views_handler_thumbnail_layer($fieldinfo, $fielddata, $value, $data) {

//	var_dump($fieldinfo, $fielddata, $value, $data);

	$video = _op_video_load($value);

	return _op_video_render_video($video, 'thumbnail-layer', $data->nid);
}


function _op_video_views_handler_thumbnail_link($fieldinfo, $fielddata, $value, $data) {

	$video = _op_video_load($value);
	$dimensions = unserialize($fielddata['options']);
	
	return _op_video_render_preview_image_link($video, $data->nid, $dimensions['width'], $dimensions['height']);
}


function _op_video_dimensions_form() {

	return array(
    'width' => array(
      '#type' => 'textfield',
      '#size' => 3,
      '#maxlength' => 3
    ),
    'height' => array(
      '#type' => 'textfield',
      '#size' => 3,
      '#maxlength' => 3
    ),
		'#type' => 'op_video_dimensions_output',
    '#process' => array('_op_video_dimensions_form_process' => array()),
    '#after_build' => array('_op_video_dimensions_form_after_build')
  );
}


function _op_video_dimensions_form_process($element) {
  $values = unserialize($element['#default_value']);
  if (!is_array($values)) {
    // set default values for options that have no stored value.
    $values = array('width' => 160, 'height' => 120);
  }
  $element['width']['#default_value'] = $values['width'];
  $element['height']['#default_value'] = $values['height'];
  return $element;
}


function _op_video_dimensions_form_after_build($element) {
  $values['width'] = $element['width']['#value'];
  $values['height'] = $element['height']['#value'];
  $element['#value'] = serialize($values);
  form_set_value($element, $element['#value']);
  return $element;
}


function theme_op_video_dimensions_output($element) {
  return $element['#children'];
}


function _op_video_duration_form() {

	return array(
    'quantity' => array(
      '#type' => 'textfield',
      '#size' => 2,
      '#maxlength' => 2
    ),
    'unit' => array(
      '#type' => 'select',
      '#options' => array(
        'm' => t('Minutes'),
        'h' => t('Hours'),
        'd' => t('Days'),
        'w' => t('Weeks'),
        'M' => t('Months')
      )
    ),
		'#type' => 'op_video_duration_output',
    '#process' => array('_op_video_duration_form_process' => array()),
    '#after_build' => array('_op_video_duration_form_after_build')
  );
}


function _op_video_duration_form_process($element) {
  $values = unserialize($element['#default_value']);
  if (!is_array($values)) {
    // set default values for options that have no stored value.
    $values = array('quantity' => 1, 'unit' => 'week');
  }
  $element['quantity']['#default_value'] = $values['quantity'];
  $element['unit']['#default_value'] = $values['unit'];
  return $element;
}


function _op_video_duration_form_after_build($element) {
  $values['quantity'] = $element['quantity']['#value'];
  $values['unit'] = $element['unit']['#value'];
  $element['#value'] = serialize($values);
  form_set_value($element, $element['#value']);
  return $element;
}


function theme_op_video_duration_output($element) {
  return $element['#children'];
}


function _op_video_views_handler_stats($fieldinfo, $fielddata, $value, $data) {
	
	switch($fielddata['field']) {
	
		case 'views_duration':
			$type = 'view';
			break;

		case 'views_completed_duration':
			$type = 'view completed';
			break;

		case 'downloads_duration':
			$type = 'download';
			break;
	}

	$values = unserialize($fielddata['options']);

	switch($values['unit']) {
		
		case 'm':
			$duration = $values['quantity'] * 60;
			break;

		case 'h':
			$duration = $values['quantity'] * 60 * 60;
			break;

		case 'd':
			$duration = $values['quantity'] * 24 * 60 * 60;
			break;

		case 'w':
			$duration = $values['quantity'] * 7 * 24 * 60 * 60;
			break;

		case 'M':
			$duration = round($values['quantity'] * 30.4369 * 24 * 60 * 60);
			break;
	}

	return _op_video_stat_count($type, $duration, $value);
}


function _op_video_views_handler_bytesize($fieldinfo, $fielddata, $value, $data) {
	
	if($value)
		return _op_video_format_bytesize($value, 2);
	else
		return t('n/a');
}


function _op_video_views_handler_duration($fieldinfo, $fielddata, $value, $data) {
	
	if($value)
		return t('%time', array('%time' => format_interval($value, is_numeric($fielddata['options']) ? $fielddata['options'] : 2)));
	else
		return t('n/a');
}



